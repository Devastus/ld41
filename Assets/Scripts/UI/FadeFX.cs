﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeFX : MonoBehaviour {
    public enum FadeType
    {
        FadeIn,
        FadeOut
    }

    RawImage fader;
    public FadeType fadeType;
    IEnumerator routine;

    public void Fade(FadeType type, float duration)
    {
        if (fader == null) fader = GetComponent<RawImage>();
        routine = FadeRoutine(type, duration);
        StartCoroutine(routine);
    }

    public IEnumerator FadeRoutine(FadeType type, float duration)
    {
        //float baseAlpha = fader.color.a;
        //float alpha;
        float timer = 0f;
        float transition = 0f;
        if (fader == null) fader = GetComponent<RawImage>();
        Color newColor = fader.color;
        while (true)
        {
            newColor.a = Mathf.Clamp01(type == FadeType.FadeIn ? 1 - transition : transition);
            fader.color = newColor;
            timer += Time.unscaledDeltaTime;
            transition = Mathf.Clamp01(timer / duration);
            if (transition >= 1)
            {
                newColor.a = type == FadeType.FadeIn ? 0 : 1;
                fader.color = newColor;
                yield break;
            }
            yield return null;
        }
    }
}
