﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBall : Ball {

    [Header("Player Settings")]
    public int health = 100;
    public int maxHealth = 100;
    public AudioClip launchBallSound;
    public AudioClip deathSound;

    private bool immune = false;
    private float immuneTimer = 0f;
    private float immuneCooldown = 0.5f;

    private bool _dead = false;
    public bool Dead { get { return _dead; } }

    public override void Initialize()
    {
        base.Initialize();
        _dead = false;
        health = maxHealth;
    }

    private void Update()
    {
        if (immune)
        {
            immuneTimer += Time.deltaTime;
            if(immuneTimer >= immuneCooldown)
            {
                immune = false;
                immuneTimer = 0;
            }
        }
    }

    public void TakeDamage(Vector3 direction, float force, uint damage)
    {
        if (!immune && !_dead)
        {
            health = (int)Mathf.Clamp(health - damage, 0, maxHealth);
            Launch(direction, force);
            HUDPanel.OnHealthChange(health, maxHealth);
            HUDPanel.OnGetHit();
            if(health <= 0)
            {
                Die();
            } else
            {
                immune = true;
            }
        }
    }

    public void GetPocketed(Pocket pocket)
    {
        if(GameManager.LevelManager.State == LevelManager.LevelState.Build)
        {
            uint damage = (uint)(health * 0.5f);
            health = (int)Mathf.Clamp(health - damage, 0, maxHealth);
            GameManager.LevelManager.PocketPlayerBall(pocket);
            HUDPanel.OnHealthChange(health, maxHealth);
            HUDPanel.OnGetHit();
        } else
        {
            health = 0;
            Die();
        }
    }

    private void Die()
    {
        _dead = true;
        HUDPanel.OnHealthChange(health, maxHealth);
        //EXPLODE
        MainCameraController.OnWiggle(transform.position, 2f);
        LevelState.OnLoseGame();
    }
}
