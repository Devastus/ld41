﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable, CreateAssetMenu(menuName = "Music Library")]
public class MusicLibrary : ScriptableObject {

    public AudioClip[] songs;

    public AudioClip GetSong(string name)
    {
        int length = songs.Length;
        for (int i = 0; i < length; i++)
        {
            if(songs[i].name == name)
            {
                return songs[i];
            }
        }
        return null;
    }
}
