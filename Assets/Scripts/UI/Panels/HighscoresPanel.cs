﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighscoresPanel : MenuPanel {

    public Text[] nameTexts;
    public Text[] scoreTexts;

    void OnEnable()
    {
        HighscoreManager.OnDownloadHighscores += PopulateScores;
    }

    void OnDisable()
    {
        HighscoreManager.OnDownloadHighscores -= PopulateScores;
    }

    public void ShowScores(bool show)
    {
        Show(show);
        if (show)
        {
            GetScores();
        }
    }

    void GetScores()
    {
        //HighscoreManager scoreMan = (HighscoreManager)GameManager.GetManager(Manager.HighscoreManager);
        GameManager.HighscoreManager.DownloadHighscores();
    }

    void PopulateScores(Highscore[] scores)
    {
        int scoresLength = scores.Length;
        for (int i = 0; i < 15; i++)
        {
            string number = (i+1) + ". ";
            string name = "";
            string score = "";
            if(i < scoresLength)
            {
                //Update with a name and a score
                name = scores[i].userName;
                score = scores[i].score.ToString();
            } else
            {
                //Update with an empty name and 0
                name = "---";
                score = "0";
            }
            nameTexts[i].text = number + name;
            scoreTexts[i].text = score; 
        }
    }
}
