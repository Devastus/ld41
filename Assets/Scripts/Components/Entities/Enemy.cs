﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour {

    public PlayerBall target;
    public SpriteRenderer spriteRenderer;
    public float speed = 3f;
    public int maxHealth = 10;
    public uint score = 100;
    public AudioClip explosionSound;
    public AudioClip deathSound;
    
    public uint damage = 1;
    public float pushForce = 2f;
    public float explodeRadius = 1f;

    private NavMeshAgent agent;
    private Animator anim;
    private int health;
    private bool _dead = false;
    public bool Dead { get { return _dead; } }

    public Color damageColor;
    private Color _baseColor;
    private float damageTimer = 0f;
    private float damageDuration = 0.3f;

    private void Awake()
    {
        _baseColor = spriteRenderer.color;
    }

    private void OnEnable()
    {
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponentInChildren<Animator>();
    }

    public void Initialize(PlayerBall player)
    {
        _dead = false;
        target = player;
        health = maxHealth;
        agent.speed = speed;
        spriteRenderer.color = _baseColor;
        if (!anim.isInitialized) anim.Rebind();
    }

    // Update is called once per frame
    void Update () {
		if(target != null && !target.Dead)
        {
            agent.SetDestination(target.transform.position);
            Vector3 direction = agent.velocity.normalized;
            Vector3 camDir = Camera.main.transform.forward;
            float dot = Mathf.Round(Vector3.Dot(direction, camDir) + 1);
            anim.SetFloat("Direction", dot);
            float sideDot = Vector3.Dot(Camera.main.transform.right, direction);

            spriteRenderer.flipX = sideDot < 0 ? true : false;
            float distance = (target.transform.position - transform.position).sqrMagnitude;
            if(distance <= (explodeRadius * explodeRadius))
            {
                anim.SetTrigger("Explode");
                Explode();
            }
        }
        if(damageTimer > 0)
        {
            float t = damageTimer / damageDuration;
            spriteRenderer.color = Color.Lerp(_baseColor, damageColor, t);
            damageTimer -= Time.deltaTime;
        } else
        {
            spriteRenderer.color = _baseColor;
        }
	}

    public void TakeDamage(Vector3 direction, float force, uint damage)
    {
        if (!_dead)
        {
            health = (int)Mathf.Clamp(health - damage, 0, maxHealth);
            damageTimer = damageDuration;
            //MainCameraController.OnWiggle(transform.position, 0.2f);
            if (health <= 0) Die();
        }
    }

    void Die()
    {
        //PARTICLE FXXX
        //GameManager.SoundManager.PlaySFX(deathSound, true, transform, 0.7f, 1f, 0.2f);
        _dead = true;
        target = null;
        //agent.Stop();
        GameManager.ParticleManager.SpawnParticle(ParticleManager.ParticleType.BloodSplatter, transform.position);
        MainCameraController.OnWiggle(transform.position, 0.2f);
        GameManager.LevelManager.GetScore(score);
        GameManager.LevelManager.DespawnEnemy(this);
    }

    void Explode()
    {
        //PARTICLE FXXXX
        //GameManager.SoundManager.PlaySFX(explosionSound, true, transform, 0.7f, 1f, 0.2f);
        target.TakeDamage(transform.forward, pushForce, damage);
        target = null;
        _dead = true;
        GameManager.ParticleManager.SpawnParticle(ParticleManager.ParticleType.Explosion, transform.position);
        //agent.Stop();
        MainCameraController.OnWiggle(transform.position, 0.5f);
        GameManager.LevelManager.DespawnEnemy(this);
    }

}
