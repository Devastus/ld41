﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleArenaCamera : MonoBehaviour {

    public enum State
    {
        PlayerAim,
        PlayerBallMoving,
        AIThinking,
        AIBallMoving,
        Collision,
        BattleOver
    }

    private const float MAX_WIGGLE = 5f;

    public static System.Action<State> OnSetState;
    public static System.Action<Transform> OnSetTarget;
    public static System.Action<Vector3, float> OnWiggle;
    public static System.Action<Transform, Transform> OnCollision;

    public Transform target;
    public float distance;
    public Vector3 direction;
    public float speed;
    public float rotationSpeed;

    [Header("Aiming Settings")]
    public float aimingDistance;
    public Vector3 aimingDirection;

    [Header("Glory Cam Settings")]
    public float gloryDistance;
    public Vector2 xAxisRange;
    public Vector2 yAxisRange;
    private Transform _collisionTarget1;
    private Transform _collisionTarget2;
    private Vector3 _gloryDirection;

    [Header("Wiggle FX")]
    public float wiggleSpeed = 1f;
    public float wiggleIntensity = 1f;
    public float wiggleFalloffSpeed = 1f;

    private float _wiggleFactor = 0f;
    private float _wiggleTimer = 0f;

    private Vector3 _targetPosition;
    private Vector3 _wiggleVector = Vector3.zero;
    private Quaternion _targetRotation;
    private Quaternion _wiggleRotation;

    private bool _gloryCamera = false;
    private float _gloryTimer = 0f;
    private float _gloryTimeLimit = 1.5f;
    private State _state = State.PlayerAim;

    private void OnEnable()
    {
        OnSetState = SetState;
        OnCollision = Collision;
        OnSetTarget = SetTarget;
        OnWiggle += ApplyWiggle;
    }

    private void OnDisable()
    {
        OnSetState = null;
        OnCollision = null;
        OnSetTarget = null;
        OnWiggle -= ApplyWiggle;
    }

    // Update is called once per frame
    void LateUpdate () {
        UpdateWiggle();
        if (_gloryCamera)
        {
            _gloryTimer += Time.unscaledDeltaTime;
            float t = _gloryTimer / _gloryTimeLimit;
            Time.timeScale = 0.3f + (t * 0.7f);
            if (_gloryTimer > _gloryTimeLimit)
            {
                _gloryCamera = false;
                _gloryTimer = 0f;
                Time.timeScale = 1f;
            }
        }
        StateLogic();
        transform.position = _targetPosition;
        transform.rotation = _targetRotation;
        //if (target != null)
        //{
            
        //    //_targetRotation = !_gloryCamera ? Quaternion.Euler(direction) : Quaternion.Euler(_gloryDirection);
        //    ////_targetRotation = Quaternion.LookRotation((target.position - transform.position).normalized, Vector3.up);
        //    //float d = !_gloryCamera ? distance : gloryDistance;
        //    //_targetPosition = (!_gloryCamera ? target.position : _gloryTarget.position) + (_targetRotation * Vector3.up) * d;
        //    //_wiggleRotation = Quaternion.Euler(_wiggleVector);

        //    //transform.position = Vector3.Lerp(transform.position, _targetPosition, Time.deltaTime * speed);

        //    //transform.rotation = Quaternion.Slerp(transform.rotation, _targetRotation, Time.deltaTime * rotationSpeed);
        //    //transform.rotation = Quaternion.LookRotation((target.position - transform.position).normalized, Vector3.up) * _wiggleRotation;
        //}
    }

    private void StateLogic()
    {
        switch (_state)
        {
            case State.PlayerAim:
                if(target != null)
                {
                    _targetRotation = target.rotation;
                    _targetPosition = target.position + (_targetRotation * aimingDirection) * aimingDistance;
                }
                break;
            case State.PlayerBallMoving:
                if(target != null)
                {
                    _targetRotation = Quaternion.Slerp(_targetRotation, Quaternion.LookRotation((target.position - transform.position).normalized, Vector3.up), Time.deltaTime * rotationSpeed);
                    _targetPosition = Vector3.Lerp(_targetPosition, target.position + direction.normalized * distance, speed * Time.deltaTime);
                }
                break;
            case State.Collision:
                Vector3 midPos = _collisionTarget1.position + _collisionTarget2.position * 0.5f;
                float collidersDistance = midPos.magnitude * 1.5f;
                _targetRotation = Quaternion.LookRotation((midPos - transform.position).normalized, Vector3.up);
                _targetPosition = Vector3.Lerp(_targetPosition, midPos + Quaternion.Euler(_gloryDirection) * Vector3.up * collidersDistance, speed * Time.deltaTime);
                break;
            default: break;
        }
    }

    void SetTarget(Transform t)
    {
        target = t;
    }

    void SetState(State state)
    {
        this._state = state;
    }

    void SetGloryCam(Transform target)
    {
        _collisionTarget1 = target;
        _gloryCamera = true;
        _gloryDirection = new Vector3(Random.Range(xAxisRange.x, xAxisRange.y), Random.Range(yAxisRange.x, yAxisRange.y), 0);
    }

    void ApplyWiggle(Vector3 position, float force)
    {
        _wiggleFactor = Mathf.Clamp(_wiggleFactor + force, 0, MAX_WIGGLE);
    }

    void UpdateWiggle()
    {
        _wiggleTimer = (_wiggleTimer + (Time.deltaTime * wiggleSpeed)) % (Mathf.PI * 2 * wiggleSpeed);
        _wiggleFactor = Mathf.Clamp(_wiggleFactor - (Time.deltaTime * wiggleFalloffSpeed), 0, MAX_WIGGLE);
        _wiggleVector = new Vector3(Mathf.Sin(_wiggleTimer * 0.81f),
                                   Mathf.Sin((_wiggleTimer + 0.04f) * 0.78f),
                                   Mathf.Sin((_wiggleTimer + 0.07f) * 0.62f)) * _wiggleFactor * wiggleIntensity;
    }

    private void Collision(Transform target1, Transform target2)
    {
        _state = State.Collision;
        _gloryCamera = true;
        _gloryDirection = new Vector3(Random.Range(xAxisRange.x, xAxisRange.y), Random.Range(yAxisRange.x, yAxisRange.y), 0);
        _collisionTarget1 = target1;
        _collisionTarget2 = target2;
        _gloryTimer = 0;
        target = null;
    }
}
