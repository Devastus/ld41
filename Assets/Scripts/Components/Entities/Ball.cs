﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {

    public System.Action OnCollision;
    public System.Action OnStopMoving;
    protected const float MAX_GRAVITY = 20f;
    protected const float SLEEP_THRESHOLD = 1f;

    protected Rigidbody _rigidBody;
    public Rigidbody Rigidbody { get { if (_rigidBody == null) _rigidBody = GetComponent<Rigidbody>(); return _rigidBody; } }
    protected SphereCollider _collider;
    public SphereCollider Collider { get { if (_collider == null) _collider = GetComponent<SphereCollider>(); return _collider; } }
    protected bool _moving = false;
    public bool Moving { get { return _moving; } }
    protected bool _grounded = false;
    public bool Grounded { get { return _grounded; } }

    [Header("Physics Settings")]
    public LayerMask terrainLayerMask;
    public LayerMask collisionLayerMask;
    public float mass = 1f;
    public float defaultFriction = 0.012f;
    public float skinWidth = 0.002f;

    protected RaycastHit _groundHit;
    protected RaycastHit _collisionHit;
    protected Vector3 _velocity;
    public Vector3 Velocity { get { return _velocity; } }
    protected Vector3 _targetPosition;

    public virtual void Initialize()
    {

    }

    protected void FixedUpdate()
    {
        if (_moving)
        {
            _targetPosition = Rigidbody.position + _velocity * Time.deltaTime;
            //CheckGrounded();
            CheckCollision();
            _velocity.y = 0;

            Rigidbody.position = _targetPosition;
            if (_velocity.sqrMagnitude < SLEEP_THRESHOLD)
            {
                _velocity = Vector3.zero;
                _moving = false;
                if (OnStopMoving != null) OnStopMoving();
            } else
            {
                _velocity *= (1 - defaultFriction);
            }
        }
    }

    public void Launch(Vector3 direction, float force)
    {
        _velocity = new Vector3(direction.x, 0f, direction.z) * force;
        _moving = true;
    }

    public void Stop()
    {
        _velocity = Vector3.zero;
        _moving = false;
    }

    public void Set(Vector3 position)
    {
        _targetPosition = position;
        transform.position = position;
    }

    //protected void CheckGrounded()
    //{
    //    float yDistance = Mathf.Abs(velocity.y) * Time.deltaTime;
    //    if (Physics.Raycast(transform.position, Vector3.down, out groundHit, yDistance + Collider.radius + skinWidth, terrainLayerMask))
    //    {
    //        _grounded = true;
    //        float diff = yDistance - groundHit.distance;
    //        if (diff > 0f)
    //        {
    //            targetPosition.y = (Rigidbody.position.y) - diff * Time.deltaTime;
    //        }
    //        velocity.y = 0;
    //        velocity *= (1 - defaultFriction);
    //    }
    //    else
    //    {
    //        _grounded = false;
    //        velocity.y = Mathf.Clamp(velocity.y + Physics.gravity.y * Time.deltaTime, -MAX_GRAVITY, MAX_GRAVITY * 3);
    //    }
    //}

    protected void CheckCollision()
    {
        float unModifiedMagnitude = _velocity.magnitude;
        float distance = unModifiedMagnitude * Time.deltaTime;
        Vector3 direction = _velocity.normalized;
        if (Physics.SphereCast(transform.position, Collider.radius, direction, out _collisionHit, distance, collisionLayerMask))
        {
            float diff = distance - _collisionHit.distance;
            if (diff > -0.0001f)
            {
                Ball opponent = _collisionHit.collider.GetComponent<Ball>();
                if (opponent != null)
                {
                    Vector3 opponentDir = (opponent.transform.position - transform.position).normalized;
                    Vector3 cross = new Vector3(opponentDir.z, 0f, -opponentDir.x);
                    float dotProduct = Mathf.Clamp(Vector3.Dot(direction, opponentDir), 0, 1);
                    float crossDot = Vector3.Dot(direction, cross);
                    opponent.Launch(opponentDir, unModifiedMagnitude * dotProduct);
                    _velocity = Mathf.Sign(crossDot) * cross * (unModifiedMagnitude * (1-dotProduct));
                    _targetPosition = Rigidbody.position + _velocity * Time.deltaTime;
                    //Vector3 newVelocity = new Vector3((_velocity.x * (mass - opponent.mass) + (2 * opponent.mass * opponent.Velocity.x)) / (mass + opponent.mass),
                    //                                  (_velocity.y * (mass - opponent.mass) + (2 * opponent.mass * opponent.Velocity.y)) / (mass + opponent.mass),
                    //                                  (_velocity.z * (mass - opponent.mass) + (2 * opponent.mass * opponent.Velocity.z)) / (mass + opponent.mass));
                    //Vector3 opponentVelocity = new Vector3((opponent.Velocity.x * (opponent.mass - mass) + (2 * mass * _velocity.x)) / (opponent.mass + mass),
                    //                                       (opponent.Velocity.y * (opponent.mass - mass) + (2 * mass * _velocity.y)) / (opponent.mass + mass),
                    //                                       (opponent.Velocity.z * (opponent.mass - mass) + (2 * mass * _velocity.z)) / (opponent.mass + mass));
                    //_velocity = newVelocity;

                    MainCameraController.OnSetTarget(opponent.transform);
                } else
                {
                    
                    Vector3 reflect = Vector3.Reflect(direction, _collisionHit.normal);
                    _targetPosition = Rigidbody.position + direction * diff;
                    _velocity = reflect * unModifiedMagnitude;
                }
            }
        }
    }
}
