﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleFSM;

public class LevelState : State
{
    public static System.Action OnLoseGame;
    private bool isGameOver = false;
    private bool isPaused = false;
    float timer = 0f;

    protected override IEnumerator Init(SimpleStateMachine seq)
    {
        isGameOver = false;
        isPaused = false;
        OnLoseGame = LoseGame;
        timer = 0f;
        GameManager.UIManager.ShowWindowSingle(1);
        GameManager.SoundManager.ChangeMusic("Robocalypse_MainTheme", 2f, Tween.Type.Linear);
        return base.Init(seq);
    }

    protected override IEnumerator Run(SimpleStateMachine seq)
    {
        GameManager.LevelManager.Initialize();
        while (true)
        {
            if (isGameOver) yield break;
            if (Input.GetButtonDown("Menu"))
            {
                isPaused = !isPaused;
                GameManager.UIManager.gameWindow.pausePanel.Show(isPaused);
                Time.timeScale = isPaused ? 0f : 1f;
            }
            if (!isPaused)
            {
                if (GameManager.LevelManager.State == LevelManager.LevelState.Wave)
                {
                    if (GameManager.LevelManager.SpawnedUnitCount < GameManager.LevelManager.MaxUnitCount)
                    {
                        if (timer >= GameManager.LevelManager.unitSpawnInterval)
                        {
                            GameManager.LevelManager.SpawnEnemy();
                            timer %= GameManager.LevelManager.unitSpawnInterval;
                            //Debug.Log(GameManager.LevelManager.SpawnedUnitCount + ", " + GameManager.LevelManager.enemies.Count);
                        }
                        timer += Time.deltaTime;
                    }
                    else if (GameManager.LevelManager.KilledUnitCount == GameManager.LevelManager.MaxUnitCount && !GameManager.LevelManager.IsAnyBallMoving())
                    {
                        GameManager.LevelManager.ChangeState();
                    }
                }
            } else
            {
                if (Input.GetKeyDown(KeyCode.R))
                {
                    GameManager.LevelManager.Clear();
                    GameManager.UIManager.gameWindow.pausePanel.Show(false);
                    Time.timeScale = 1f;
                    seq.Transition(1);
                    yield break;
                }
            }
            yield return null;
        }
    }

    protected override IEnumerator End(SimpleStateMachine seq)
    {
        //Animate a slow fadeout as the player explodes, then switch over to the Highscores screen
        OnLoseGame = null;
        GameManager.SoundManager.ChangeMusic(null, 2f, Tween.Type.Linear);
        GameManager.LevelManager.Clear();
        return base.End(seq);
    }

    private void LoseGame()
    {
        _activeTransitionIndex = 0;
        isGameOver = true;
    }
}
