﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SoundManager {

    [Range(1,128)]
    public int audioSourceLimit = 16;
    public bool soundOn = true;
    public bool musicOn = true;
    public bool is2D = true;
    [Range(0f,1f)]
    public float sfxVolume = 1f;
    [Range(0f, 1f)]
    public float musicVolume = 0.75f;
    public MusicLibrary musicLibrary;

    GameObject audioObjectHolder;
    AudioObject[] audioObjects;
    int _currentAudioObject;

    AudioSource music1;
    AudioSource music2;
    int _currentMusicSource = 0;
    

    public void Initialize()
    {
        audioObjectHolder = new GameObject("Audio Objects");
        audioObjects = new AudioObject[audioSourceLimit];
        for (int i = 0; i < audioSourceLimit; i++)
        {
            audioObjects[i] = AudioObject.Create(audioObjectHolder.transform, is2D);
        }
        music1 = audioObjectHolder.AddComponent<AudioSource>();
        music1.volume = musicVolume;

        music1.bypassEffects = true;
        music1.bypassListenerEffects = true;
        music1.bypassReverbZones = true;
        music1.loop = true;

        music2 = audioObjectHolder.AddComponent<AudioSource>();
        music2.volume = musicVolume;

        music2.bypassEffects = true;
        music2.bypassListenerEffects = true;
        music2.bypassReverbZones = true;
        music2.loop = true;
    }

    public void ChangeMusic(string name, float fadeDuration, Tween.Type fadeType)
    {
        ChangeMusic(name != null ? GetSong(name) : null, fadeDuration, fadeType);
    }

    private void ChangeMusic(AudioClip song, float fadeDuration, Tween.Type fadeType)
    {
        if (fadeDuration <= 0)
        {
            GetCurrentMusicSource().clip = song;
            GetCurrentMusicSource().Play();
        } else
        {
            GameManager.Instance.StartCoroutine(Crossfade(song, fadeDuration, fadeType));
        }
    }

    public void StopMusic()
    {
        if (music1.isPlaying) music1.Stop();
        if (music2.isPlaying) music2.Stop();
    }

    //public void PlaySFX(string sfxName, bool randomizePitch = false, Transform emitter = null, float volume = 1f, float pitch = 1f, float pitchRange = 0f)
    //{
    //    AudioClip clip = library.GetSound(sfxName);
    //    PlaySFX(clip, randomizePitch, emitter, volume, pitch, pitchRange);
    //}

    /// <summary>
    /// Play a Sound Effect (Can be positioned by inserting an emitter Transform object)
    /// </summary>
    /// <param name="clip">Audio Clip to play</param>
    /// <param name="randomizePitch">Randomize the pitch of the clip?</param>
    /// <param name="emitter">Does this sound have an emitter?</param>
    /// <param name="volume">The volume to play this Clip in</param>
    /// <param name="pitch">The actual pitch of the clip</param>
    /// <param name="pitchRange">If being randomized, the range to randomize the pitch in</param>
    public void PlaySFX(AudioClip clip, bool randomizePitch = false, Transform emitter = null, float volume = 1f, float pitch = 1f, float pitchRange = 0f)
    {
        if (soundOn)
        {
            float p = pitch;
            if (randomizePitch)
            {
                p = Random.Range(pitch - pitchRange, pitch + pitchRange);
            }
            audioObjects[_currentAudioObject].Play(clip, emitter, volume * sfxVolume, p);
            _currentAudioObject = (_currentAudioObject + 1) % audioSourceLimit;
        }
    }

    private IEnumerator Crossfade(AudioClip clip, float duration, Tween.Type fadeType)
    {
        float timer = 0f;
        float volume1 = music1.volume;
        float volume2 = music2.volume;
        if(_currentMusicSource == 0)
        {
            music2.clip = clip;
            if(clip != null)music2.Play();
        } else
        {
            music1.clip = clip;
            if (clip != null) music1.Play();
        }
        float goal1 = _currentMusicSource == 0 ? 0f : musicVolume;
        float goal2 = _currentMusicSource == 0 ? musicVolume : 0f;
        while(timer < duration)
        {
            music1.volume = Tween.Transition(volume1, goal1, ref timer, duration, fadeType);
            music2.volume = Tween.Transition(volume2, goal2, ref timer, duration, fadeType);
            yield return null;
        }
        music1.volume = goal1;
        music2.volume = goal2;
    }

    private AudioClip GetSong(string name)
    {
        return musicLibrary.GetSong(name);
    }

    private AudioSource GetCurrentMusicSource()
    {
        switch (_currentMusicSource)
        {
            case 0:
                return music1;
            case 1:
                return music2;
            default:
                return null;
        }
    }
}

public class AudioObject
{
    public GameObject obj;
    public AudioSource audio;
    public Transform tr;

    public static AudioObject Create(Transform parent, bool is2D)
    {
        AudioObject ao = new AudioObject();
        ao.obj = new GameObject("Audio Object");
        ao.tr = ao.obj.transform;
        ao.tr.SetParent(parent);
        ao.audio = ao.obj.AddComponent<AudioSource>();
        ao.audio.spatialBlend = is2D ? 0f : 1f;
        ao.audio.rolloffMode = AudioRolloffMode.Linear;
        ao.audio.maxDistance = 60;
        return ao;
    }

    public void Play(AudioClip clip, Transform emitter = null, float volume = 1f, float pitch = 1f)
    {
        audio.clip = clip;
        audio.volume = volume;
        audio.pitch = pitch;
        if (emitter != null)
        {
            tr.position = emitter.position;
            tr.SetParent(emitter);
        }
        audio.Play();
    }
}