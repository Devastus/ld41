﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[System.Serializable]
public class SceneController {

    public int SceneCount { get { return SceneManager.sceneCountInBuildSettings; } }

    public IEnumerator LoadAsync(string sceneName)
    {
        if (!SceneManager.GetSceneByName(sceneName).isLoaded)
        {
            var async = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
            yield return async;
        }
    }

	public void Load(string sceneName, bool async = true)
    {
        if (!SceneManager.GetSceneByName(sceneName).isLoaded)
        {
            if (async)
            {
                SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
            } else
            {
                SceneManager.LoadScene(sceneName);
            }
        }
    }

    public void Unload(string sceneName, bool async = true)
    {
        if (SceneManager.GetSceneByName(sceneName).isLoaded)
        {
            if (async)
            {
                SceneManager.UnloadSceneAsync(sceneName);
            } else
            {
                SceneManager.UnloadScene(sceneName);
            }
        }
    }
}
