﻿Shader "Custom/CustomSky"
{
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float2 uv : TEXCOORD0;
				float3 wPos : TEXCOORD1;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;

			uniform half4 _SkyColor;
			uniform half4 _HorizonColor;
			uniform half4 _GroundColor;

			uniform fixed _Darken;
			uniform fixed _GroundSkyTransition;
			uniform half _HorizonDensity;

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.wPos = mul(unity_ObjectToWorld, v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				return o;
			}

			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col;
				half3 vPos = normalize(i.wPos);
				col = lerp(_SkyColor, _SkyColor  * _SkyColor * (1-_Darken), saturate(vPos.y) * (_Darken));
				col = lerp(col, _GroundColor, 1 - saturate(pow(vPos.y * 0.5 + 1, _GroundSkyTransition * 16)));
				col = lerp(col, _HorizonColor, pow(1 - abs(vPos.y), _HorizonDensity * 16));
				return col;
			}
			ENDCG
		}
	}
}
