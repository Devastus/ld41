﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool {

    public const int UNLIMITED_COUNT = 0;

    public GameObject prefab;
    public int initialCount;
    public int maxCount;

    Stack<GameObject> pooledObjects;
    int spawnedObjects = 0;

    public ObjectPool (GameObject prefab, int initialCount, int maxCount = UNLIMITED_COUNT)
    {
        pooledObjects = new Stack<GameObject>();
        this.prefab = prefab;
        this.initialCount = initialCount;
        this.maxCount = maxCount;
    }
    
    public GameObject Spawn(Vector3 position, Quaternion rotation, Transform parent = null, System.Action spawnDelegate = null)
    {
        GameObject obj;
        if(pooledObjects.Count == 0 && (maxCount == UNLIMITED_COUNT || spawnedObjects <= maxCount))
        {
            obj = GameManager.Instantiate(prefab, position, rotation, parent) as GameObject;
            obj.name = prefab.name + " " + pooledObjects.Count+1;
            obj.AddComponent<PoolMember>().myPool = this;
            spawnedObjects++;
        } else
        {
            obj = pooledObjects.Pop();
            //if (obj == null) return Spawn(position, rotation, parent);
        }
        obj.transform.position = position;
        obj.transform.rotation = rotation;
        obj.transform.parent = parent;
        if (spawnDelegate != null) spawnDelegate();
        obj.SetActive(true);
        return obj;
    }

    public void Despawn(GameObject obj)
    {
        if(obj != null)
        {
            obj.SetActive(false);
            pooledObjects.Push(obj);
        }
    }
}

[System.Serializable]
public class PoolMember : MonoBehaviour
{
    public ObjectPool myPool;
}
