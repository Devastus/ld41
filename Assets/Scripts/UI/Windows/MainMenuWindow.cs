﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuWindow : MenuWindow {

    public GameObject instructions;

	public void StartGame()
    {
        MainMenuState.OnStartGame();
    }

    public void CheckHighscores()
    {
        GameManager.UIManager.ShowWindowSingle(2);
        GameManager.UIManager.leaderboardWindow.ShowLeaderboards(false);
    }

    public void CheckInstructions(bool active)
    {
        instructions.SetActive(active);
    }
}
