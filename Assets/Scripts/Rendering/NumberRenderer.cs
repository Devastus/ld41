﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NumberRenderer : MonoBehaviour {

    public Sprite[] numbers;

    private SpriteRenderer _renderer;
    public SpriteRenderer Renderer { get { if (_renderer == null) _renderer = GetComponent<SpriteRenderer>(); return _renderer; } }

    /// <summary>
    /// Set a one-digit number to be rendered.
    /// </summary>
    /// <param name="number">The number to be rendered.</param>
    public void SetNumber(int number)
    {
        if(number < numbers.Length)
        {
            Renderer.sprite = numbers[number];
        }
    }
}
