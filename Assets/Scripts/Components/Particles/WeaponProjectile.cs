﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponProjectile : MonoBehaviour {

    protected uint damage;
    protected float projectileSpeed;
    protected float range;
    protected float force;
    protected Transform origin;
    protected Enemy target;
    protected float t = 0;
    protected Vector3 direction;

    //protected SpriteRenderer _renderer;
    //public SpriteRenderer Renderer { get { if (_renderer == null) _renderer = GetComponent<SpriteRenderer>(); return _renderer; } }

    public static void Spawn(GameObject projectilePrefab, Transform origin, Enemy target, uint damage, float force, float projectileSpeed, float range)
    {
        WeaponProjectile projectile = ObjectPoolManager.Spawn(projectilePrefab, origin.position, origin.rotation).GetComponent<WeaponProjectile>();
        projectile.Init(origin, target, damage, force, projectileSpeed, range);
    }

    protected virtual void Init(Transform origin, Enemy target, uint damage, float force, float projectileSpeed, float range)
    {
        this.origin = origin;
        this.target = target;
        this.damage = damage;
        this.force = force;
        this.projectileSpeed = projectileSpeed;
        this.range = range;
        this.transform.position = origin.position;
        this.direction = (target.transform.position - origin.position).normalized;
        this.t = 0;
    }

    protected void Despawn()
    {
        ObjectPoolManager.Despawn(this.gameObject);
    }
}
