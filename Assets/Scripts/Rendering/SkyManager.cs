﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class SkyManager : MonoBehaviour {

    public static System.Action<SkyManagerSettings> OnPropertyChange; //The "OnValidate" hook for Sky Manager Settings

    public SkyManagerSettings skyManagerSettings;

    int skyColorID;
    int horizonColorID;
    int groundColorID;
    int darkenID;
    int groundSkyTransitionID;
    int horizonDensityID;

    void OnEnable()
    {
        skyColorID = Shader.PropertyToID(SkyManagerSettings.skyColorLabel);
        horizonColorID = Shader.PropertyToID(SkyManagerSettings.horizonColorLabel);
        groundColorID = Shader.PropertyToID(SkyManagerSettings.groundColorLabel);
        darkenID = Shader.PropertyToID(SkyManagerSettings.darkenLabel);
        groundSkyTransitionID = Shader.PropertyToID(SkyManagerSettings.groundSkyTransitionLabel);
        horizonDensityID = Shader.PropertyToID(SkyManagerSettings.horizonDensityLabel);
        OnPropertyChange = ApplySkyProperties;
        ApplySkyProperties(skyManagerSettings);
    }

    void OnDisable()
    {
        OnPropertyChange = null;
    }

    public void ApplySkyProperties(SkyManagerSettings s)
    {
        if(skyManagerSettings != null && skyManagerSettings == s)
        {
            Shader.SetGlobalColor(skyColorID, skyManagerSettings.skyColor);
            Shader.SetGlobalColor(horizonColorID, skyManagerSettings.horizonColor);
            Shader.SetGlobalColor(groundColorID, skyManagerSettings.groundColor);
            Shader.SetGlobalFloat(darkenID, skyManagerSettings.darken);
            Shader.SetGlobalFloat(groundSkyTransitionID, skyManagerSettings.groundSkyTransition);
            Shader.SetGlobalFloat(horizonDensityID, skyManagerSettings.horizonDensity);
            if (skyManagerSettings.affectAmbientLight)
            {
                if(skyManagerSettings.ambientLightingType == SkyManagerSettings.AmbientLightType.Trilight)
                {
                    RenderSettings.ambientMode = UnityEngine.Rendering.AmbientMode.Trilight;
                    RenderSettings.ambientSkyColor = skyManagerSettings.skyColor * skyManagerSettings.ambientIntensity;
                    RenderSettings.ambientEquatorColor = skyManagerSettings.horizonColor * skyManagerSettings.ambientIntensity;
                    RenderSettings.ambientGroundColor = skyManagerSettings.groundColor * skyManagerSettings.ambientIntensity;
                } else
                {
                    RenderSettings.ambientMode = UnityEngine.Rendering.AmbientMode.Flat;
                    RenderSettings.ambientLight = skyManagerSettings.skyColor * skyManagerSettings.ambientIntensity;
                }
            } else
            {
                RenderSettings.ambientMode = UnityEngine.Rendering.AmbientMode.Skybox;
            }
        }
    }

    void OnValidate()
    {
        if(skyManagerSettings != null)
            ApplySkyProperties(skyManagerSettings);
    }
}
