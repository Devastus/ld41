﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ParticleManager {

    public enum ParticleType
    {
        BloodSplatter,
        Explosion
    }

    [SerializeField]
    private ParticleFX[] particleFX;

    public void SpawnParticle(ParticleType type, Vector3 position)
    {
        ParticleFX fx = particleFX[(int)type];
        if (fx != null) fx.Spawn(position);
    }
}
