﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RailgunProjectile : WeaponProjectile {

    private LineRenderer _renderer;
    public LineRenderer Renderer { get { if (_renderer == null) _renderer = GetComponent<LineRenderer>(); return _renderer; } }
    public LayerMask layerMask;
    public float distance = 500f;
    public float radius = 2f;
    private RaycastHit[] hits;

    protected override void Init(Transform origin, Enemy target, uint damage, float force, float projectileSpeed, float range)
    {
        base.Init(origin, target, damage, force, projectileSpeed, range);
        direction = (new Vector3(target.transform.position.x, origin.position.y, target.transform.position.z) - origin.position).normalized;
        Renderer.widthMultiplier = radius;
        hits = Physics.SphereCastAll(origin.position, radius, direction, distance, layerMask, QueryTriggerInteraction.Ignore);
        int length = hits.Length;
        for (int i = 0; i < length; i++)
        {
            hits[i].collider.GetComponent<Enemy>().TakeDamage(direction, force, damage);
        }
        Renderer.SetPosition(0, new Vector3(origin.position.x, origin.position.y + 1f, origin.position.z));
        Vector3 endPos = origin.position + direction * distance;
        Renderer.SetPosition(1, new Vector3(endPos.x, endPos.y + 1f, endPos.z));
    }

    private void Update()
    {
        if (t < 1f)
        {
            Renderer.startColor = new Color(Renderer.startColor.r, Renderer.startColor.g, Renderer.startColor.b, 1 - t);
            Renderer.endColor = new Color(Renderer.endColor.r, Renderer.endColor.g, Renderer.endColor.b, 1 - t);
            t += Time.deltaTime * projectileSpeed;
        }
        else
        {
            Despawn();
        }
    }
}
