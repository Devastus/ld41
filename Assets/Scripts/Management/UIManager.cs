﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class UIManager {

    public MainMenuWindow mainMenuWindow;
    public GameWindow gameWindow;
    public LeaderboardWindow leaderboardWindow;
    //public CinematicWindow cinematicWindow;
    //public GlobalWindow globalWindow;

    public void ShowWindow(int windowIndex, bool show)
    {
        switch (windowIndex)
        {
            case 0:
                mainMenuWindow.Show(show);
                break;
            case 1:
                gameWindow.Show(show);
                break;
            case 2:
                leaderboardWindow.Show(show);
                break;
                //case 3:
                //    cinematicWindow.Show(show);
                //    break;
        }
    }

    public void ShowWindowSingle(int windowIndex)
    {
        switch (windowIndex)
        {
            case 0:
                mainMenuWindow.Show(true);
                gameWindow.Show(false);
                leaderboardWindow.Show(false);
                //cinematicWindow.Show(false);
                return;
            case 1:
                mainMenuWindow.Show(false);
                gameWindow.Show(true);
                leaderboardWindow.Show(false);
                //cinematicWindow.Show(false);
                ////globalWindow.Show(false);
                return;
            case 2:
                mainMenuWindow.Show(false);
                gameWindow.Show(false);
                leaderboardWindow.Show(true);
                //    cinematicWindow.Show(false);
                //    ////globalWindow.Show(true);
                return;
                //case 3:
                //    mainMenuWindow.Show(false);
                //    gameWindow.Show(false);
                //    leaderboardsWindow.Show(false);
                //   /cinematicWindow.Show(true);
                //    return;
        }
    }
}
