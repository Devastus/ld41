﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogCondition : MonoBehaviour {

    public Text answer;
    public Image selectedImage;
    private bool selected = false;

    public void Select(bool select)
    {
        selected = select;
        selectedImage.enabled = selected;
    }
}
