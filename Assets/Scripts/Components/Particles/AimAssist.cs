﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimAssist : MonoBehaviour {

    public LayerMask layerMask;
    private LineRenderer _lineRenderer;
    public LineRenderer LineRenderer { get { if (_lineRenderer == null) _lineRenderer = GetComponent<LineRenderer>(); return _lineRenderer; } }
    private MeshRenderer _renderer;
    public MeshRenderer Renderer { get { if (_renderer == null) _renderer = GetComponentInChildren<MeshRenderer>(); return _renderer; } }

    private RaycastHit hit;

    public void SetPosition(Transform origin, Vector3 chargePos, float height)
    {
        transform.position = origin.position;
        transform.rotation = Quaternion.LookRotation((chargePos - origin.position).normalized, Vector3.up);
    }

    public void SetCharge(float value)
    {
        Renderer.sharedMaterial.SetFloat("_Threshold", value);
    }

    public void SetLine(Transform origin, Vector3 mousePos, float height, float radius)
    {
        Vector3 originPos = new Vector3(origin.position.x, origin.position.y + height, origin.position.z);
        Vector3 correctedMousePos = new Vector3(mousePos.x, mousePos.y + height, mousePos.z);
        LineRenderer.SetPosition(0, originPos);
        //Do a raycast
        Vector3 diff = correctedMousePos - originPos;
        Vector3 dir = (diff).normalized;
        float distance = diff.magnitude;
        if(Physics.SphereCast(originPos, radius, dir, out hit, distance, layerMask, QueryTriggerInteraction.Ignore))
        //if (Physics.Raycast(originPos, dir, out hit, distance, layerMask, QueryTriggerInteraction.Ignore))
        {
            //We hit something collidable
            LineRenderer.positionCount = 3;
            Vector3 originCollisionPos = hit.point + hit.normal * radius;
            LineRenderer.SetPosition(1, originCollisionPos);
            Ball ball = hit.collider.GetComponent<Ball>();
            if (ball != null)
            {
                //Vector3 colliderCenter = hit.point - hit.normal * radius;
                //Vector3 originCenter = hit.point + hit.normal * radius;
                Vector3 cross = -new Vector3(hit.normal.z, 0f, -hit.normal.x);
                float dot = Mathf.Clamp(Vector3.Dot(dir, -hit.normal), 0, 1);
                float crossDot = Vector3.Dot(dir, cross);
                //Vector3 colliderCenter = ball.transform.position + ball.Collider.center;
                //Vector3 colliderDir = (colliderCenter - hit.point).normalized;
                //Vector3 originCenter = hit.point - colliderDir * radius;
                //Vector3 cross = new Vector3(colliderDir.z, 0f, -colliderDir.x);
                //float dot = Mathf.Clamp(Vector3.Dot(dir, colliderDir), 0, 1);
                //float crossDot = Vector3.Dot(dir, cross);
                Vector3 newDirection = Mathf.Sign(crossDot) * cross * (1 - dot);
                newDirection.y = 0f;
                LineRenderer.SetPosition(2, hit.point + newDirection * 75f);
            }
            else
            {
                Vector3 newDirection = Vector3.Reflect(dir, hit.normal);
                LineRenderer.SetPosition(2, hit.point + newDirection * 50f);
            }
        }
        else
        {
            //A straight line
            LineRenderer.positionCount = 2;
            LineRenderer.SetPosition(1, originPos + dir * distance);
        }
        //Gradient gradient = new Gradient();
        //gradient.
        //Renderer.colorGradient = gradient;
    }

    public void SetChargeVisible(bool visible)
    {
        Renderer.enabled = visible;
    }

    public void SetActive(bool active)
    {
        gameObject.SetActive(active);
    }
}
