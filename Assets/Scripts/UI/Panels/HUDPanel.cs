﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDPanel : MenuPanel {

    public static System.Action<int> OnTryCountChange;
    public static System.Action<int, int> OnHealthChange;
    public static System.Action OnGetHit;
    public static System.Action<uint> OnScoreChange;
    public static System.Action<uint> OnWaveChange;

    public Image healthBar;
    public Text scoreText;
    public Text waveText;
    public Image[] tryCountImages;
    public CanvasGroup damageFX;

    private float _damageFXAlpha;
    private float _damageFXTimer = 0f;
    private float _damageFXDuration = 0.4f;

    private void OnEnable()
    {
        OnTryCountChange = SetTryCount;
        OnHealthChange = SetHealth;
        OnScoreChange = SetScore;
        OnWaveChange = SetWave;
        OnGetHit = SetDamageFX;
    }

    private void OnDisable()
    {
        OnTryCountChange = null;
        OnHealthChange = null;
        OnScoreChange = null;
        OnWaveChange = null;
        OnGetHit = null;
    }

    private void Update()
    {
        if(_damageFXTimer > 0)
        {
            _damageFXAlpha = _damageFXTimer / _damageFXDuration;
            damageFX.alpha = _damageFXAlpha;
            _damageFXTimer -= Time.unscaledDeltaTime;
        } else
        {
            _damageFXAlpha = 0;
            damageFX.alpha = 0;
        }
    }

    public void SetTryCount(int count)
    {
        for (int i = 0; i < 3; i++)
        {
            if (i >= count)
            {
                tryCountImages[i].enabled = false;
            } else
            {
                tryCountImages[i].enabled = true;
            }
        }
    }

    public void SetHealth(int value, int maxValue)
    {
        healthBar.fillAmount = value / (float)maxValue;
    }

    public void SetDamageFX()
    {
        _damageFXTimer = _damageFXDuration;
    }

    public void SetScore(uint value)
    {
        scoreText.text = value.ToString();
    }

    public void SetWave(uint wave)
    {
        waveText.text = "Wave " + wave.ToString();
    }
}
