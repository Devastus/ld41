﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleFSM;

public class GameOverState : State {

    protected override IEnumerator Init(SimpleStateMachine seq)
    {
        GameManager.UIManager.ShowWindowSingle(2);
        GameManager.UIManager.leaderboardWindow.ShowLeaderboards(true);
        return base.Init(seq);
    }

    protected override IEnumerator Run(SimpleStateMachine seq)
    {
        bool sent = false;
        GameManager.UIManager.leaderboardWindow.addScorePanel.ShowScore(true);
        while (!sent)
        {
            GameManager.UIManager.leaderboardWindow.addScorePanel.SelectInputField();
            if (Input.GetButtonDown("Submit"))
            {
                sent = GameManager.UIManager.leaderboardWindow.addScorePanel.TrySend();
            }
            yield return null;
        }
        GameManager.UIManager.leaderboardWindow.addScorePanel.Show(false);
        GameManager.UIManager.leaderboardWindow.ShowLeaderboards(false);
        while (true)
        {
            if (Input.GetButtonDown("Submit") || Input.GetButtonDown("Interact") || Input.GetButtonDown("Menu") || Input.GetMouseButtonDown(0)) break;
            yield return null;
        }
        GameManager.UIManager.leaderboardWindow.Show(false);
    }

    protected override IEnumerator End(SimpleStateMachine seq)
    {
        return base.End(seq);
    }
}
