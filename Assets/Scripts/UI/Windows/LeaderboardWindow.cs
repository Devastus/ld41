﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeaderboardWindow : MenuWindow {

    public HighscoresPanel highscorePanel;
    public AddNewScorePanel addScorePanel;

	public void ShowLeaderboards(bool addNew)
    {
        if (addNew)
        {
            highscorePanel.ShowScores(false);
            addScorePanel.ShowScore(true);
        } else
        {
            addScorePanel.ShowScore(false);
            highscorePanel.ShowScores(true);
        }
    }
}
