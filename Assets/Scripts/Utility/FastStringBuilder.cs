﻿using UnityEngine;
using System.Text;
using System.Collections;

public class FastStringBuilder {

    StringBuilder _builder;
    //int _capacity;
    //int _maxCapacity;
    string _str;

    public int Length { get { return _builder.Length; } }

    public FastStringBuilder(int capacity, int maxCapacity)
    {
        _builder = new StringBuilder(capacity, maxCapacity);
        //this._capacity = capacity;
        //this._maxCapacity = maxCapacity;
        _str = (string)_builder.GetType().GetField(
                "_str", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance)
                .GetValue(_builder);
    }

    public void Append(string value)
    {
        _builder.Append(value);
    }

    public void Append(char value)
    {
        _builder.Append(value);
    }

    public void Append(int value)
    {
        _builder.Append(value);
    }

    public void Append(uint value)
    {
        _builder.Append(value);
    }

    public void Append(float value)
    {
        _builder.Append(value);
    }

    public void Append(double value)
    {
        _builder.Append(value);
    }

    public void Append(bool value)
    {
        _builder.Append(value);
    }

    public void Clear()
    {
        int capacity = _builder.Capacity;
        for (int i = 0; i < capacity; i++)
        {
            _builder.Replace(_str[i], default(char));
        }
        _builder.Length = 0;
    }

    public void Set(string value)
    {
        Clear();
        Append(value);
    }

    public void Set(char value)
    {
        Clear();
        Append(value);
    }

    public void Set(int value)
    {
        Clear();
        Append(value);
    }

    public void Set(uint value)
    {
        Clear();
        Append(value);
    }

    public void Set(float value)
    {
        Clear();
        Append(value);
    }

    public void Set(double value)
    {
        Clear();
        Append(value);
    }

    public void Set(bool value)
    {
        Clear();
        Append(value);
    }

    public override string ToString()
    {
        return _str;
    }
}
