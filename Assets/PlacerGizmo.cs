﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlacerGizmo : MonoBehaviour {

    public Color validColor;
    public Color invalidColor;
    public bool isValid = false;

    private MeshRenderer _renderer;
    public MeshRenderer Renderer { get { if (_renderer == null) _renderer = GetComponentInChildren<MeshRenderer>(); return _renderer; } }

    private void Update()
    {
        if (isValid)
        {
            Renderer.sharedMaterial.color = validColor;
        } else
        {
            Renderer.sharedMaterial.color = invalidColor;
        }
    }

    public void SetActive(bool active)
    {
        gameObject.SetActive(active);
    }
}
