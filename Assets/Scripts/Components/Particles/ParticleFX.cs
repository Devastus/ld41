﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleFX : MonoBehaviour {

    public static System.Action<Vector3> OnSpawnParticle;
    public int particleCount = 100;
    public AudioClip sound;
    public float volume;
    public float pitchRange;
    ParticleSystem p;

    void OnEnable()
    {
        OnSpawnParticle += Spawn;
    }

    void OnDisable()
    {
        OnSpawnParticle -= Spawn;
    }

    public void Spawn(Vector3 position)
    {
        transform.position = position;
        if (p == null) p = GetComponentInChildren<ParticleSystem>();
        p.Emit(particleCount);
        if (sound != null) GameManager.SoundManager.PlaySFX(sound, true, null, volume, 1f, pitchRange);
        //p.Play();
    }

    public void Play()
    {
        if (p == null) p = GetComponentInChildren<ParticleSystem>();
        //p.Play();
        p.Emit(particleCount);
        if (sound != null) GameManager.SoundManager.PlaySFX(sound, true, null, volume, 1f, pitchRange);
        //p.
    }
}
