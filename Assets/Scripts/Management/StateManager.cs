﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleFSM;

[System.Serializable]
public class StateManager {

    [SerializeField]
    public SimpleStateMachine stateMachine;
    private Coroutine runRoutine;

    public void Start()
    {
        runRoutine = GameManager.Instance.StartCoroutine(stateMachine.Run(GameManager.Instance));
    }

    public void Stop()
    {
        GameManager.Instance.StopCoroutine(runRoutine);
    }
}
