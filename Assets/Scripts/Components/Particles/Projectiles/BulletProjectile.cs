﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletProjectile : WeaponProjectile {

    private SpriteRenderer _renderer;
    public SpriteRenderer Renderer { get { if (_renderer == null) _renderer = GetComponent<SpriteRenderer>(); return _renderer; } }

    private void Update()
    {
        if (t < 1f)
        {
            transform.position = Vector3.Lerp(origin.position, target.transform.position, t);
            t += Time.deltaTime * projectileSpeed;
        }
        else
        {
            direction = (target.transform.position - origin.position).normalized;
            target.TakeDamage(direction, force, damage);
            Despawn();
        }
    }
}
