﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SimpleFSM
{
    public struct Transition
    {
		public enum Type {
			 Safe,
			 Overwrite
		 }
		
        public State from;
        public State to;
        public int stateIndex;
		public Type type;

        public Transition(State from, State to, int stateIndex, Type type = Type.Safe)
        {
            this.from = from;
            this.to = to;
            this.stateIndex = stateIndex;
			this.type = type;
        }
    }
}

