﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretBall : Ball {

    public const int MAX_LEVEL = 9;

    [Header("Turret Settings")]
    public Weapon weapon;
    public Enemy targetEnemy;
    public Transform gunPoint;
    public SpriteRenderer spriteRenderer;
    public TrailRenderer trailRenderer;

    private int _level = 1;
    public int Level { get { return _level; } }
    private bool _assembled = false;
    public bool Assembled { get { return _assembled; } }

    private Animator anim;
    private float _targetingTimer = 0f;
    private float _targetingInterval = 0.5f;
    private float _weaponCooldown = 0f;

    private void OnEnable()
    {
        if (anim == null) anim = GetComponentInChildren<Animator>();
    }

    private void OnDrawGizmosSelected()
    {
        if(weapon != null)
        {
            Gizmos.DrawWireSphere(transform.position, weapon.Range);
        }
    }

    public override void Initialize()
    {
        base.Initialize();
        //Get basic machine gun
        _level = 1;
        weapon = GameManager.LevelManager.weaponDatabase.GetWeapon(Weapon.Type.MachineGun);
        weapon.Initialize(1);
        spriteRenderer.color = weapon.weaponColor;
        trailRenderer.startColor = weapon.weaponColor;
        trailRenderer.endColor = weapon.weaponColor;
        trailRenderer.Clear();
        _weaponCooldown = 0;
        if (!anim.isInitialized) anim.Rebind();
    }

    public void Reset(Vector3 position)
    {
        Stop();
        trailRenderer.Clear();
        //Reset the turret state (spawning animation etc.) and position it
        //Get the new sprite set
        Set(position);
        anim.SetTrigger("Spawn");
    }

    public void Upgrade(Weapon.Type turretType)
    {
        _level = Mathf.Clamp(_level + 1, 1, MAX_LEVEL);
        weapon = GameManager.LevelManager.weaponDatabase.GetWeapon(turretType);
        weapon.Initialize(_level);
        spriteRenderer.color = weapon.weaponColor;
        trailRenderer.startColor = weapon.weaponColor;
        trailRenderer.endColor = weapon.weaponColor;
    }

    private void Update()
    {
        if (_assembled)
        {
            if(_targetingTimer >= _targetingInterval)
            {
                targetEnemy = GameManager.LevelManager.GetClosestEnemy(transform.position);
                _targetingTimer %= _targetingInterval;
            }
            _targetingTimer += Time.deltaTime;

            if(targetEnemy != null && !targetEnemy.Dead)
            {
                float dist = (targetEnemy.transform.position - transform.position).sqrMagnitude;
                if (dist <= weapon.Range * weapon.Range)
                {
                    if(_weaponCooldown <= 0)
                    {
                        weapon.Shoot(gunPoint, targetEnemy);
                        _weaponCooldown = weapon.WeaponSpeed;
                    }
                    
                }
            }
            if (_weaponCooldown > 0f) _weaponCooldown -= Time.deltaTime;
        }
    }

    public void Assemble(bool assemble)
    {
        _assembled = assemble;
        targetEnemy = null;
        anim.SetBool("Assembled", assemble);
        _targetingTimer = 0f;
        _weaponCooldown = 0f;
    }
}
