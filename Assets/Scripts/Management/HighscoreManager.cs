﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighscoreManager {

    /* NOTE: This simple Highscore Manager stores and loads highscores using dreamlo leaderboard website and HTTP GET requests.
     */

    public static System.Action<Highscore[]> OnDownloadHighscores;
    public static System.Action<string, int> OnAddNewHighscore;

    const string privateURL = "http://dreamlo.com/lb/wiJghEygcEK9UwuJC5B5og_Yir9vEK8EKb6S5GPHeaVw";
    const string privateCode = "wiJghEygcEK9UwuJC5B5og_Yir9vEK8EKb6S5GPHeaVw";
    const string publicCode = "5ade2cf7d6024519e0ef3c3c";
    const string webURL = "http://dreamlo.com/lb/";


    protected void Initialize()
    {
        OnAddNewHighscore += AddNewHighscore;
    }

    protected void Close()
    {
        OnAddNewHighscore -= AddNewHighscore;
    }

    public void AddNewHighscore(string name, int score)
    {
        GameManager.Instance.StartCoroutine(UploadHighscore(name, score));
    }

    public void AddNewHighscore(string name, uint score)
    {
        GameManager.Instance.StartCoroutine(UploadHighscore(name, (int)score));
    }

    public void DownloadHighscores()
    {
        GameManager.Instance.StartCoroutine(DownloadFromDatabase());
    }

    IEnumerator UploadHighscore(string name, int score)
    {
        WWW www = new WWW(webURL + privateCode + "/add/" + WWW.EscapeURL(name) + "/" + score);
        yield return www;

        if (string.IsNullOrEmpty(www.error))
        {
            Debug.Log("HighscoreManager: New Highscore upload successful.");
        }
        else
        {
            Debug.Log("HighscoreManager: Error uploading: " + www.error);
        }
    }

    IEnumerator DownloadFromDatabase()
    {
        WWW www = new WWW(webURL + publicCode + "/pipe/");
        yield return www;

        if (string.IsNullOrEmpty(www.error))
        {
            FormatHighscore(www.text);
        }
        else
        {
            Debug.Log("HighscoreManager: Error downloading: " + www.error);
        }
    }

    void FormatHighscore(string textStream)
    {
        string[] entries = textStream.Split(new char[] { '\n' }, System.StringSplitOptions.RemoveEmptyEntries);
        int length = entries.Length;
        Highscore[] highscores = new Highscore[length];
        for (int i = 0; i < length; i++)
        {
            string[] entryInfo = entries[i].Split(new char[] { '|' });
            entryInfo[0] = entryInfo[0].Replace('+', ' ');
            highscores[i] = new Highscore(entryInfo[0], int.Parse(entryInfo[1]));
        }
        if (OnDownloadHighscores != null) OnDownloadHighscores(highscores);
    }
}

public struct Highscore
{
    public string userName;
    public int score;

    public Highscore(string userName, int score)
    {
        this.userName = userName;
        this.score = score;
    }
}
