﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct BitBool {

    private byte _data;

    public BitBool(bool one = false, bool two = false, bool three = false, bool four = false, bool five = false, bool six = false, bool seven = false, bool eight = false)
    {
        _data = 0;
        this[0] = one;
        this[1] = two;
        this[2] = three;
        this[3] = four;
        this[4] = five;
        this[5] = six;
        this[6] = seven;
        this[7] = eight;
    }

    public bool this[int index]
    {
        get
        {
            if(index< 0 || index> 7) throw new System.IndexOutOfRangeException();
            return (_data & (1 << index)) > 0;
        }
        set
        {
            if(index< 0 || index> 7) throw new System.IndexOutOfRangeException();
            if(value)
                _data |= (byte)(1 << index);
            else
                _data &= (byte)~(1 << index);
        }
    }
}
