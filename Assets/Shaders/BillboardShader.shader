﻿Shader "Custom/BillboardShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_ScaleX ("Scale X", Float) = 1.0
		_ScaleY ("Scale Y", Float) = 1.0
		_LightingInfluence ("Lighting Influence", Range(0.0,1.0)) = 0.0
	}
	SubShader
	{
		Tags { "Queue"="Transparent" "RenderType"="Transparent" "IgnoreProjector" = "True" "DisableBatching" = "True" }
		LOD 100

		Blend SrcAlpha OneMinusSrcAlpha
		ZWrite Off
		Cull Off

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			#include "Lighting.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float4 color : COLOR;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 color : COLOR;
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			uniform fixed _ScaleX;
			uniform fixed _ScaleY;
			uniform fixed _LightingInfluence;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_P, mul(UNITY_MATRIX_MV, float4(0.0,0.0,0.0,1))
				+ float4(v.vertex.x, v.vertex.y, 0.0, 0.0)
				* float4(_ScaleX, _ScaleY, 1.0, 1.0));
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.color = v.color;
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv) * i.color;
				fixed3 lighting = col * (UNITY_LIGHTMODEL_AMBIENT);
				col.rgb = lerp(col.rgb, lighting, _LightingInfluence);
				return col;
			}
			ENDCG
		}
	}
}
