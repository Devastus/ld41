﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "SkyManager/SkyManagerSettings")]
public class SkyManagerSettings : ScriptableObject {

    public enum AmbientLightType
    {
        Flat,
        Trilight
    }

    public const string skyColorLabel = "_SkyColor";
    public const string groundColorLabel = "_GroundColor";
    public const string horizonColorLabel = "_HorizonColor";
    public const string darkenLabel = "_Darken";
    public const string groundSkyTransitionLabel = "_GroundSkyTransition";
    public const string horizonDensityLabel = "_HorizonDensity";

    [Header("Sky Properties")]
    public Color skyColor;
    public Color horizonColor;
    public Color groundColor;
    [Range(0f, 1f)]
    public float darken = 0.5f;
    [Range(0f, 20f)]
    public float groundSkyTransition = 2f;
    [Range(0f, 10f)]
    public float horizonDensity = 2f;

    [Header("Indirect Lighting")]
    public bool affectAmbientLight = true;
    public AmbientLightType ambientLightingType = AmbientLightType.Trilight;
    [Range(0f, 1f)]
    public float ambientIntensity = 0.34f;

    void OnValidate()
    {
        if(SkyManager.OnPropertyChange != null)
            SkyManager.OnPropertyChange(this);
    }
}
