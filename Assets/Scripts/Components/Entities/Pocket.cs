﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pocket : MonoBehaviour {

    public Transform pocketSpawn;
    public SpriteRenderer pointerRenderer;
    public WeaponDatabase weaponDatabase;

    private void OnTriggerEnter(Collider other)
    {
        Ball ball = other.GetComponent<Ball>();
        if(ball != null)
        {
            Debug.Log("Pocket was entered");
            System.Type type = ball.GetType();
            if (type == typeof(PlayerBall))
            {
                PlayerBall player = (PlayerBall)ball;
                player.GetPocketed(this);
            } else
            {
                TurretBall turret = (TurretBall)ball;
                GameManager.LevelManager.UpgradeTurret(turret, upgradeType);
            }
        }
    }

    public Weapon.Type upgradeType;

	public void Randomize()
    {
        upgradeType = (Weapon.Type)Random.Range(1, 6);
    }

    public void AssignType(Weapon.Type type)
    {
        upgradeType = type;
        switch (type)
        {
            default:
                pointerRenderer.enabled = false;
                break;
            case Weapon.Type.MachineGun:
                pointerRenderer.enabled = true;
                pointerRenderer.color = weaponDatabase.GetWeapon(type).weaponColor;
                break;
            case Weapon.Type.Laser:
                pointerRenderer.enabled = true;
                pointerRenderer.color = weaponDatabase.GetWeapon(type).weaponColor;
                break;
            case Weapon.Type.Railgun:
                pointerRenderer.enabled = true;
                pointerRenderer.color = weaponDatabase.GetWeapon(type).weaponColor;
                break;
        }
    }
}
