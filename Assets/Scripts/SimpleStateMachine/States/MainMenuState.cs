﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleFSM;

public class MainMenuState : State {

    public static System.Action OnStartGame;
    private bool isStarting = false;

    protected override IEnumerator Init(SimpleStateMachine seq)
    {
        GameManager.UIManager.ShowWindowSingle(0);
        OnStartGame += StartGame;
        isStarting = false;
        return base.Init(seq);
    }

    protected override IEnumerator Run(SimpleStateMachine seq)
    {
        while (true)
        {
            if (GameManager.UIManager.leaderboardWindow.Visible)
            {
                if (Input.GetButtonDown("Submit") || Input.GetButtonDown("Interact") || Input.GetButtonDown("Menu") || Input.GetMouseButtonDown(0)) GameManager.UIManager.ShowWindowSingle(0);
            }
            else if (GameManager.UIManager.mainMenuWindow.instructions.activeSelf == true)
            {
                if (Input.anyKeyDown) GameManager.UIManager.mainMenuWindow.CheckInstructions(false);
            }
            if (isStarting) yield break;
            yield return null;
        }
    }

    protected override IEnumerator End(SimpleStateMachine seq)
    {
        OnStartGame -= StartGame;
        return base.End(seq);
    }

    public void StartGame()
    {

        isStarting = true;
    }
}
