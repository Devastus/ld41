﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class AddNewScorePanel : MenuPanel
{
    public Text yourScoreText;
    public InputField enterNameField;
    int score;

    EventSystem eventSystem { get { return GameObject.Find("EventSystem").GetComponent<EventSystem>(); } }

    //void Update()
    //{
    //    if (Input.GetButtonDown("Submit"))
    //    {
    //        string name = enterNameField.text;
    //        if (name != string.Empty)
    //        {
    //            SendScore(name);
    //        }
    //        else
    //        {
    //            //YOU NEED TO ENTER A NAME
    //        }
    //    }
    //}

    public void SelectInputField()
    {
        eventSystem.SetSelectedGameObject(enterNameField.gameObject);
    }

    public void ShowScore(bool show)
    {
        Show(show);
        if (show)
        {
            //DataManager dataMan = (DataManager)GameManager.GetManager(Manager.DataManager);
            //score = dataMan.characterStats.totalStrExp;
            score = (int)GameManager.LevelManager.Score;
            yourScoreText.text = "Your score: " + score;
        }
    }

    public bool TrySend()
    {
        string name = enterNameField.text;
        if (name != string.Empty)
        {
            SendScore(name);
            return true;
        }
        else
        {
            return false;
        }
    }

    void SendScore(string name)
    {
        GameManager.HighscoreManager.AddNewHighscore(name, score);
        GameManager.UIManager.leaderboardWindow.ShowLeaderboards(false);
        //HighscoreManager scoreMan = (HighscoreManager)GameManager.GetManager(Manager.HighscoreManager);
        //SoundManager sMan = (SoundManager)GameManager.GetManager(Manager.SoundManager);
        //UIManager uiMan = (UIManager)GameManager.GetManager(Manager.UIManager);
        //scoreMan.AddNewHighscore(name, score);
        //sMan.PlaySFX("Accept");
        //uiMan.leaderboardWindow.ShowLeaderboards(false);
    }
}
