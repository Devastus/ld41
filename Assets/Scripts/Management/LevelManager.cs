﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LevelManager {

    public static System.Action<Ball> OnBallDeployed;

    public enum LevelState
    {
        Build,
        Wave
    }

    public AimAssist aimAssist;
    public GameObject playerBallPrefab;
    public GameObject turretPrefab;
    public GameObject[] enemyPrefabs;
    public Pocket[] pockets;

    [HideInInspector]
    public PlayerBall playerBall;
    [HideInInspector]
    public List<TurretBall> deployedTurrets = new List<TurretBall>();
    [HideInInspector]
    public List<Enemy> enemies = new List<Enemy>();

    public Transform enemySpawner;
    public Transform playerBallSpawner;
    public Vector2 turretBallSpawnArea;
    public WeaponDatabase weaponDatabase;

    public int baseUnitCount;
    public int baseUnitsPerSpawn;
    public uint baseTurretScoreThreshold;
    public int baseTriesCount;
    public float unitMultiplier;
    public float unitSpawnInterval;
    public float healPerLevelMultiplier = 0.25f;
    public float turretScoreThresholdMultiplier = 0.25f;

    public bool _controllable = true;
    public bool Controllable { get { return _controllable; } }

    private LevelState _state;
    public LevelState State { get { return _state; } }

    private uint _level = 0;
    public uint Level { get { return _level; } }

    private int _killedUnitCount = 0;
    public int KilledUnitCount { get { return _killedUnitCount; } }
    private int _spawnedUnitCount = 0;
    public int SpawnedUnitCount { get { return _spawnedUnitCount; } }
    private int _maxUnitCount = 0;
    public int MaxUnitCount { get { return _maxUnitCount; } }
    private int _unitsPerSpawn = 0;
    public int UnitsPerSpawn { get { return _unitsPerSpawn; } }
    private int _deployableCount = 0;
    public int DeployableCount { get { return _deployableCount; } }

    private uint _score = 0;
    public uint Score { get { return _score; } }
    private uint _turretScoreThreshold = 0;
    public uint TurretScoreThreshold { get { return _turretScoreThreshold; } }
    private int _tries = 0;
    public int Tries { get { return _tries; } }

    //private TurretBall lastPocketedTurretBall;

    public void Initialize()
    {
        _level = 1;
        _state = LevelState.Build;
        _controllable = true;
        RandomizePockets();
        _tries = baseTriesCount;
        _score = 0;
        _unitsPerSpawn = baseUnitsPerSpawn;
        _maxUnitCount = baseUnitCount;
        _turretScoreThreshold = baseTurretScoreThreshold;
        playerBall = ObjectPoolManager.Spawn(playerBallPrefab, playerBallSpawner.position, Quaternion.identity).GetComponent<PlayerBall>();
        playerBall.Initialize();
        HUDPanel.OnTryCountChange(_tries);
        HUDPanel.OnHealthChange(playerBall.health, playerBall.maxHealth);
        HUDPanel.OnScoreChange(0);
        HUDPanel.OnWaveChange(_level);
        _deployableCount = 2;
        for (int i = _deployableCount; i > 0; i--)
        {
            DeployTurret();
        }
        MainCameraController.OnSetState(MainCameraController.State.PlayerAim);
    }

    public void SpawnEnemy()
    {
        if(_spawnedUnitCount < _maxUnitCount)
        {
            int unitDiff = _maxUnitCount - _spawnedUnitCount;
            int randomCount = Random.Range((int)(_unitsPerSpawn * 0.8f), (int)(_unitsPerSpawn * 1.2f));
            int length = Mathf.Min(unitDiff, randomCount);
            for (int i = 0; i < length; i++)
            {
                int randomIndex = Random.Range(0, enemyPrefabs.Length);
                Enemy enemy = ObjectPoolManager.Spawn(enemyPrefabs[randomIndex], enemySpawner.position, Quaternion.identity).GetComponent<Enemy>();
                enemy.Initialize(playerBall);
                enemies.Add(enemy);
                _spawnedUnitCount++;
            }
        }
    }

    public void DespawnEnemy(Enemy enemy)
    {
        if(enemy != null)
        {
            enemies.Remove(enemy);
            ObjectPoolManager.Despawn(enemy.gameObject);
            _killedUnitCount++;
        }
    }

    public void ChangeState()
    {
        _state = (LevelState)((int)(_state + 1) % 2);
        switch (_state)
        {
            case LevelState.Build:
                _level++;
                if (_level > 1)
                {
                    //Heal some of the player health back
                    int slice = (int)(playerBall.health * healPerLevelMultiplier);
                    playerBall.health = (int)Mathf.Clamp(playerBall.health + slice, 0, playerBall.maxHealth);
                }
                RandomizePockets();
                _tries = baseTriesCount;
                _controllable = true;
                if (_deployableCount > 0)
                {
                    for (int i = _deployableCount; i > 0; i--)
                    {
                        DeployTurret();
                    }
                }
                {
                    int length = deployedTurrets.Count;
                    for (int i = 0; i < length; i++)
                    {
                        deployedTurrets[i].Assemble(false);
                    }
                }
                HUDPanel.OnTryCountChange(_tries);
                HUDPanel.OnWaveChange(_level);
                HUDPanel.OnHealthChange(playerBall.health, playerBall.maxHealth);
                MainCameraController.OnSetState(MainCameraController.State.PlayerAim);
                break;
            case LevelState.Wave:
                for (int i = 0; i < 6; i++)
                {
                    pockets[i].AssignType(Weapon.Type.None);
                }
                _spawnedUnitCount = 0;
                _killedUnitCount = 0;
                _maxUnitCount = (int)Mathf.Max(baseUnitCount, _maxUnitCount + (_maxUnitCount * unitMultiplier));
                _unitsPerSpawn = (int)Mathf.Max(baseUnitsPerSpawn, _unitsPerSpawn + (int)(_unitsPerSpawn * unitMultiplier));
                _controllable = false;
                {
                    int length = deployedTurrets.Count;
                    for (int i = 0; i < length; i++)
                    {
                        deployedTurrets[i].Assemble(true);
                    }
                }
                break;
        }
    }

    public void DeployTurret()
    {
        Vector2 halfSize = new Vector2(turretBallSpawnArea.x * 0.5f, turretBallSpawnArea.y * 0.5f);
        Vector3 randomPoint = new Vector3(Random.Range(-halfSize.x, halfSize.x), 0f, Random.Range(-halfSize.y, halfSize.y));
        TurretBall tb = ObjectPoolManager.Spawn(turretPrefab, randomPoint, Quaternion.identity).GetComponent<TurretBall>();
        tb.Initialize();
        deployedTurrets.Add(tb);
        _deployableCount--;
    }

    public void UpgradeTurret(TurretBall turret, Weapon.Type upgradeType)
    {
        if(upgradeType != Weapon.Type.None)
        {
            //Turret pocketed into a valid pocket, upgrading and possibly giving a new weapon type
            Vector2 halfSize = new Vector2(turretBallSpawnArea.x * 0.5f, turretBallSpawnArea.y * 0.5f);
            Vector3 randomPoint = new Vector3(Random.Range(-halfSize.x, halfSize.x), 0f, Random.Range(-halfSize.y, halfSize.y));
            turret.Upgrade(upgradeType);
            turret.Reset(randomPoint);
        } else
        {
            //Player pocketed into an empty pocket, loses the turret
            turret.Stop();
            deployedTurrets.Remove(turret);
            ObjectPoolManager.Despawn(turret.gameObject);
        }
        MainCameraController.OnSetTarget(playerBall.transform);
    }

    public void GetScore(uint value)
    {
        _score += value;
        HUDPanel.OnScoreChange(_score);
        if(_score >= _turretScoreThreshold)
        {
            _deployableCount++;
            _turretScoreThreshold += (uint)(_turretScoreThreshold * turretScoreThresholdMultiplier);
        }
    }

    public void Clear()
    {
        ObjectPoolManager.Despawn(playerBall.gameObject);
        int length = enemies.Count;
        for (int i = 0; i < length; i++)
        {
            ObjectPoolManager.Despawn(enemies[i].gameObject);
        }
        enemies.Clear();
        length = deployedTurrets.Count;
        for (int i = 0; i < length; i++)
        {
            ObjectPoolManager.Despawn(deployedTurrets[i].gameObject);
        }
        deployedTurrets.Clear();
    }

    private void RandomizePockets()
    {
        for (int i = 0; i < 6; i++)
        {
            pockets[i].AssignType(Weapon.Type.None);
        }
        List<int> indices = new List<int>(6);
        indices.Add(0); indices.Add(1); indices.Add(2); indices.Add(3); indices.Add(4); indices.Add(5);
        for (int i = 0; i < 3; i++)
        {
            int randomIndex = Random.Range(0, indices.Count);
            indices.RemoveAt(randomIndex);
        }
        for (int i = 0; i < 3; i++)
        {
            Weapon.Type type = (Weapon.Type)Random.Range(0, 3);
            pockets[indices[i]].AssignType(type);
        }
    }

    public bool UseTry()
    {
        _tries--;
        HUDPanel.OnTryCountChange(_tries);
        if (_tries <= 0)
        {
            ChangeState();
            return false;
        }
        return true;
    }

    public Vector3 GetAverageEnemyPos()
    {
        int length = enemies.Count;
        Vector3 avg = Vector3.zero;
        for (int i = 0; i < length; i++)
        {
            avg += enemies[i].transform.position;
        }
        if(avg != Vector3.zero)avg /= length;
        return avg;
    }

    public Vector3 GetAverageTurretPos()
    {
        int length = deployedTurrets.Count;
        Vector3 avg = Vector3.zero;
        for (int i = 0; i < length; i++)
        {
            avg += deployedTurrets[i].transform.position;
        }
        if (avg != Vector3.zero) avg /= length;
        return avg;
    }
    
    public Enemy GetClosestEnemy(Vector3 position)
    {
        int count = enemies.Count;
        if (count == 0) return null;
        int closest = -1;
        float cDist = float.MaxValue;
        for (int i = 0; i < count; i++)
        {
            float dist = (enemies[i].transform.position - position).sqrMagnitude;
            if(dist < cDist && !enemies[i].Dead)
            {
                closest = i;
                cDist = dist;
            }
        }
        if (closest == -1) return null;
        return enemies[closest];
    }

    public void PocketPlayerBall(Pocket pocket)
    {
        playerBall.Stop();
        playerBall.transform.position = pocket.pocketSpawn.position;
    }

    public bool IsAnyBallMoving()
    {
        bool moving = playerBall.Moving;
        if (!moving)
        {
            int length = deployedTurrets.Count;
            for (int i = 0; i < length; i++)
            {
                if (deployedTurrets[i].Moving) return true;
            }
        }
        return moving;
    }
}
