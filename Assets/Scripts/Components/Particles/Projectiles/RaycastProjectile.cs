﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastProjectile : WeaponProjectile {

    private LineRenderer _renderer;
    public LineRenderer Renderer { get { if (_renderer == null) _renderer = GetComponent<LineRenderer>(); return _renderer; } }

    protected override void Init(Transform origin, Enemy target, uint damage, float force, float projectileSpeed, float range)
    {
        base.Init(origin, target, damage, force, projectileSpeed, range);
        direction = (target.transform.position - origin.position).normalized;
        target.TakeDamage(direction, force, damage);
    }

    private void Update()
    {
        Renderer.SetPosition(0, new Vector3(origin.position.x, origin.position.y + 1f, origin.position.z));
        Renderer.SetPosition(1, new Vector3(target.transform.position.x, target.transform.position.y + 1f, target.transform.position.z));
        if(t < 1f)
        {
            Renderer.startColor = new Color(Renderer.startColor.r, Renderer.startColor.g, Renderer.startColor.b, 1-t);
            Renderer.endColor = new Color(Renderer.endColor.r, Renderer.endColor.g, Renderer.endColor.b, 1-t);
            t += Time.deltaTime * projectileSpeed;
        } else
        {
            Despawn();
        }
    }
}
