﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ObjectPoolManager {

    private const int DEFAULT_POOL_SIZE = 3;

    static Dictionary<GameObject, ObjectPool> pools;
    private static GameObject _poolObj;
    public static GameObject PoolObj { get
        {
            if(_poolObj == null)
            {
                _poolObj = new GameObject("Object Pool");
            }
            return _poolObj;
        } }

    static void Init(GameObject prefab = null, int count = DEFAULT_POOL_SIZE)
    {
        if(pools == null)
        {
            pools = new Dictionary<GameObject, ObjectPool>();
        }
        if(prefab != null && pools.ContainsKey(prefab) == false)
        {
            pools.Add(prefab, new ObjectPool(prefab, count));
        }
    }

    public static GameObject Spawn(GameObject prefab, Vector3 position, Quaternion rotation, Transform parent = null, System.Action spawnDelegate = null)
    {
        if (prefab == null)
        {
            Debug.LogError("ObjectPoolManager: trying to spawn a null GameObject!");
            return null;
        }
        Init(prefab);
        return pools[prefab].Spawn(position, rotation, parent, spawnDelegate);
    }

    public static void Despawn(GameObject obj)
    {
        if(obj != null)
        {
            PoolMember pm = obj.GetComponent<PoolMember>();
            if (pm == null)
            {
                GameManager.Destroy(obj);
            }
            else
            {
                pm.myPool.Despawn(obj);
                obj.transform.SetParent(PoolObj.transform);
            }
        }
    }
}
