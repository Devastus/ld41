﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameWindow : MenuWindow {

    public HUDPanel hudPanel;
    public PausePanel pausePanel;
    private bool _paused = false;

    public void TogglePause()
    {
        _paused = !_paused;
        if(_paused == true)
        {
            hudPanel.Show(false);
            pausePanel.Show(true);
        } else
        {
            hudPanel.Show(true);
            pausePanel.Show(false);
        }
    }
}
