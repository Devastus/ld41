﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

namespace SimpleFSM
{
    [CustomEditor(typeof(SimpleStateMachine))]
    public class SimpleStateMachineEditor : Editor
    {
        const string MAIN_PATH = "Assets/Scripts/SimpleStateMachine/";
        const string STATE_TYPE = "t:monoscript";

        private SimpleStateMachine fsm;
        private ReorderableList stateList;

        void OnEnable()
        {
            fsm = (SimpleStateMachine)target;
        }

        public override void OnInspectorGUI()
        {
            if (stateList == null) SetupStateList();
            if (stateList != null) stateList.DoLayoutList();
            EditorUtility.SetDirty(fsm);
        }

        private void SetupStateList()
        {
            stateList = new ReorderableList(fsm.states, typeof(State), true, true, true, true);
            stateList.elementHeightCallback = (int index) =>
            {
                State element = (State)stateList.list[index];
                float elementHeight = EditorGUIUtility.singleLineHeight + 5 + (element.nextStateIndices.Length * EditorGUIUtility.singleLineHeight);
                return elementHeight;
            };
            stateList.drawHeaderCallback = (Rect rect) =>
            {
                EditorGUI.LabelField(rect, "States");
            };
            stateList.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
            {
                float labelWidth = rect.width * 0.25f;
                float contentWidth = rect.width * 0.75f;
                //float halfContentWidth = contentWidth * 0.5f;
                State element = (State)stateList.list[index];

                EditorGUI.LabelField(new Rect(rect.x, rect.y + 2, rect.width - 52, EditorGUIUtility.singleLineHeight), element.name, EditorStyles.boldLabel);
                int length = element.nextStateIndices.Length;

                if (length > 0)
                {
                    string[] stateLabels = fsm.GetStateNames();
                    int[] values = GetIntegerPopupValues(stateLabels);
                    for (int i = 0; i < length; i++)
                    {
                        float y = rect.y + 2 + EditorGUIUtility.singleLineHeight + (i * EditorGUIUtility.singleLineHeight);
                        EditorGUI.LabelField(new Rect(rect.x, y, labelWidth, EditorGUIUtility.singleLineHeight), "Transition #" + (i + 1));
                        Rect popupRect = new Rect(rect.x + labelWidth, y, contentWidth, EditorGUIUtility.singleLineHeight);
                        element.nextStateIndices[i] = EditorGUI.IntPopup(popupRect, element.nextStateIndices[i], stateLabels, values);
                    }
                }

                if (GUI.Button(new Rect(rect.x + rect.width - 32, rect.y + 2, 16, 16), "-", EditorStyles.miniButtonLeft)) ResizeStateIndexArray(element, length - 1);
                if (GUI.Button(new Rect(rect.x + rect.width - 16, rect.y + 2, 16, 16), "+", EditorStyles.miniButtonRight)) ResizeStateIndexArray(element, length + 1);
            };
            stateList.onAddDropdownCallback = (Rect buttonRect, ReorderableList list) =>
            {
                var menu = new GenericMenu();
                string[] guids = AssetDatabase.FindAssets(STATE_TYPE, new string[] { MAIN_PATH + "States" });
                int length = guids.Length;
                for (int a = 0; a < length; a++)
                {
                    string path = AssetDatabase.GUIDToAssetPath(guids[a]);
                    menu.AddItem(new GUIContent(Path.GetFileNameWithoutExtension(path)), false, menuClickHandler, AssetDatabase.LoadAssetAtPath<MonoScript>(path));
                }
                menu.ShowAsContext();
            };
            stateList.onRemoveCallback = (ReorderableList list) =>
            {
                int index = list.index;
                int length = list.count;
                for (int i = 0; i < length; i++)
                {
                    State s = (State)stateList.list[i];
                    int c = s.nextStateIndices.Length;
                    for (int y = 0; y < c; y++)
                    {
                        if (s.nextStateIndices[y] == index) s.nextStateIndices[y] = State.EMPTY_TRANSITION;
                    }
                }
                State state = (State)stateList.list[index];
                fsm.states.Remove(state);
                DestroyImmediate(state, true);
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
                list.index = list.count > 0 ? list.count - 1 : 0;
            };
        }

        private void menuClickHandler(object target)
        {
            if(target != null)
            {
                MonoScript script = (MonoScript)target;
                System.Type type = script.GetClass();
                var obj = ScriptableObject.CreateInstance(type);
                State state = (State)obj;
                state.name = type.ToString();
                if (!fsm.states.Contains(state))
                {
                    fsm.states.Add(state);
                    AssetDatabase.AddObjectToAsset(state, fsm);
                    AssetDatabase.SaveAssets();
                    AssetDatabase.Refresh();
                }
            }
        }

        private void ResizeStateIndexArray(State state, int newLength)
        {
            int length = state.nextStateIndices.Length;
            int[] newIndices = new int[newLength];
            for (int i = 0; i < newLength; i++)
            {
                if(i >= length)
                {
                    newIndices[i] = State.EMPTY_TRANSITION;
                } else
                {
                    newIndices[i] = state.nextStateIndices[i];
                }
            }
            state.nextStateIndices = newIndices;
        }

        private int[] GetIntegerPopupValues(string[] labels)
        {
            int length = labels.Length;
            int[] values = new int[length];
            for (int i = 0; i < length; i++)
            {
                values[i] = i - 1;
            }
            return values;
        }
    }
}

