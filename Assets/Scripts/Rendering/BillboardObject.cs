﻿using UnityEngine;

[RequireComponent(typeof(Renderer)), ExecuteInEditMode]
public class BillboardObject : MonoBehaviour {

    public Vector2 scale = new Vector2(1, 1);

    void Awake()
    {
        SetupShaderProperties();
    }

    void OnValidate()
    {
        SetupShaderProperties();
    }

    void SetupShaderProperties()
    {
        Renderer renderer = GetComponent<Renderer>();
        renderer.sharedMaterial.SetFloat("_ScaleX", scale.x);
        renderer.sharedMaterial.SetFloat("_ScaleY", scale.y);
    }
}
