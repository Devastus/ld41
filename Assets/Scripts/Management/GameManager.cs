﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    private static GameManager _instance;
    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<GameManager>();
                if (_instance == null)
                {
                    GameObject go = new GameObject("Game Manager");
                    _instance = go.AddComponent<GameManager>();
                }
            }
            return _instance;
        }
    }

    private SceneController _sceneController = new SceneController();
    public static SceneController SceneController { get { return Instance._sceneController; } }
    [SerializeField]
    private StateManager _stateManager = new StateManager();
    public static StateManager StateManager { get { return Instance._stateManager; } }
    [SerializeField]
    private LevelManager _levelManager = new LevelManager();
    public static LevelManager LevelManager { get { return Instance._levelManager; } }
    [SerializeField]
    private ObjectPoolManager _poolManager = new ObjectPoolManager();
    public static ObjectPoolManager PoolManager { get { return Instance._poolManager; } }
    [SerializeField]
    private UIManager _uiManager = new UIManager();
    public static UIManager UIManager { get { return Instance._uiManager; } }
    [SerializeField]
    private SoundManager _soundManager = new SoundManager();
    public static SoundManager SoundManager { get { return Instance._soundManager; } }
    [SerializeField]
    private HighscoreManager _highscoreManager = new HighscoreManager();
    public static HighscoreManager HighscoreManager { get { return Instance._highscoreManager; } }
    [SerializeField]
    private ParticleManager _particleManager = new ParticleManager();
    public static ParticleManager ParticleManager { get { return Instance._particleManager; } }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireCube(Vector3.zero, new Vector3(_levelManager.turretBallSpawnArea.x, 0f, _levelManager.turretBallSpawnArea.y));
    }

    private void Start()
    {
        _stateManager.Start();
        _soundManager.Initialize();
    }
}
