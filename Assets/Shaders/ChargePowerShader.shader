﻿Shader "Custom/ChargePowerShader"
{
	Properties
	{
		_Color1("Color 1", Color) = (1.0,1.0,1.0,1.0)
		_Color2("Color 2", Color) = (1.0,1.0,1.0,1.0)
		_Intensity1("Intensity 1", Range(0,2)) = 1
		_Intensity2("Intensity 2", Range(0,2)) = 1
		_MainTex("Main Texture", 2D) = "white" {}
		_SecondTex("Second Texture", 2D) = "white" {}
		_MaskTex("Mask Texture", 2D) = "white" {}
		_Threshold ("Threshold", Range(0,1)) = 1
	}
	SubShader
	{
		Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" }
		LOD 100

		Blend SrcAlpha OneMinusSrcAlpha
		ZWrite Off
		Cull Off
		ZTest Off

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float2 maskuv : TEXCOORD1;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float2 maskuv : TEXCOORD1;
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			sampler2D _SecondTex;
			sampler2D _MaskTex;
			float4 _MainTex_ST;
			float4 _MaskTex_ST;
			fixed4 _Color1;
			fixed4 _Color2;
			fixed _Threshold;
			fixed _Intensity1;
			fixed _Intensity2;
			
			v2f vert (appdata v)
			{
				v2f o;
				//o.vertex = UnityObjectToClipPos(v.vertex);
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.maskuv = TRANSFORM_TEX(v.maskuv, _MaskTex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 mask = tex2D(_MaskTex, i.maskuv);
				//fixed threshold = smoothstep(mask - 0.04, mask + 0.04, _Threshold);
				fixed threshold = step(mask, _Threshold);
				fixed4 activeColor = tex2D(_SecondTex, i.uv) * _Color2 * _Intensity2 * threshold;
				fixed4 inactiveColor = tex2D(_MainTex, i.uv) * _Color1 * _Intensity1 * (1 - threshold);
				fixed4 col = inactiveColor + activeColor;
				return col;
			}
			ENDCG
		}
	}
}
