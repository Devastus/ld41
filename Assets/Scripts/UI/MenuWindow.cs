﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuWindow : MonoBehaviour {

    public MenuPanel[] panels;
    private bool _visible = false;
    public bool Visible { get { return _visible; } }

	public void Show(bool show)
    {
        gameObject.SetActive(show);
        _visible = show;
    }

    public void ShowPanel(int panelIndex, bool show)
    {
        int length = panels.Length;
        for (int i = 0; i < length; i++)
        {
            if (i == panelIndex)
            {
                panels[i].Show(show);
            }
        }
    }
}
