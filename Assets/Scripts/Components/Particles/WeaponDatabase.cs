﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Weapon Database")]
public class WeaponDatabase : ScriptableObject {
    public Weapon[] weapons;

    public Weapon GetWeapon(Weapon.Type type)
    {
        return weapons[(int)type];
    }
}
