﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SimpleFSM
{
    [System.Serializable]
    public abstract class State : ScriptableObject
    {
        public const int EMPTY_TRANSITION = -1;

        public enum Cycle
        {
            Init,
            Run,
            End
        }

        protected Cycle _currentCycle = Cycle.Init;
        public Cycle CurrentCycle { get { return _currentCycle; } }
        public int[] nextStateIndices = { EMPTY_TRANSITION };
        protected int _activeTransitionIndex = 0;
        public int NextStateIndex { get { return nextStateIndices[_activeTransitionIndex]; } }

        protected virtual IEnumerator Init(SimpleStateMachine seq)
        {
            yield break;
        }

        protected virtual IEnumerator Run(SimpleStateMachine seq)
        {
            yield break;
        }

        protected virtual IEnumerator End(SimpleStateMachine seq)
        {
            yield break;
        }

        public IEnumerator GetCycleRoutine(SimpleStateMachine seq)
        {
            switch (_currentCycle)
            {
                case Cycle.Init:
                    return Init(seq);
                case Cycle.Run:
                    return Run(seq);
                case Cycle.End:
                    return End(seq);
                default:
                    return null;
            }
        }

        public void NextCycle()
        {
            _currentCycle = (Cycle)(((int)_currentCycle + 1) % 3);
        }

        public void ResetCycle()
        {
            _currentCycle = Cycle.Init;
        }

        public override bool Equals(object obj)
        {
            State s = (State)obj;
            return Equals(s);
        }

        public bool Equals(State s)
        {
            return this.name == s.name;
        }
    }
}

