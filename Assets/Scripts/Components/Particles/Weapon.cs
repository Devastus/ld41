﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Weapon {

    public enum Type
    {
        None = -1,
        MachineGun,
        Laser,
        Railgun,
    }

    public string name;
    public uint baseDamage;
    public float damageMultiplier = 1f;
    public float baseAimSpeed;
    public float aimSpeedMultiplier = 1f;
    public float baseRange;
    public float rangeMultiplier = 1f;
    public float baseWeaponSpeed;
    public float weaponSpeedMultiplier = 1f;
    public float projectileSpeed = 1f;
    public float weaponForce = 1f;
    public GameObject weaponFX;
    public Color weaponColor;
    public AudioClip weaponSound;
    [Range(0f,1f)]public float weaponSoundVolume = 1f;
    [Range(0f,1f)]public float weaponSoundPitchRange = 0.2f;

    private uint _damage;
    public uint Damage { get { return _damage; } }
    private float _aimSpeed;
    public float AimSpeed { get { return _aimSpeed; } }
    private float _range;
    public float Range { get { return _range; } }
    private float _weaponSpeed;
    public float WeaponSpeed { get { return _weaponSpeed; } }

    public void Initialize(int level)
    {
        _damage = baseDamage + (uint)(baseDamage * damageMultiplier * (level - 1));
        _aimSpeed = baseAimSpeed + (baseAimSpeed * aimSpeedMultiplier * (level - 1));
        _range = baseRange + (baseRange * rangeMultiplier * (level - 1));
        _weaponSpeed = baseWeaponSpeed * Mathf.Pow(weaponSpeedMultiplier, (level - 1));
    }

    public void Shoot(Transform origin, Enemy target)
    {
        WeaponProjectile.Spawn(weaponFX, origin, target, _damage, weaponForce, projectileSpeed, _range);
        GameManager.SoundManager.PlaySFX(weaponSound, true, origin, weaponSoundVolume, 1f, weaponSoundPitchRange);
    }
}
