﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Tween {

    public enum Type
    {
        Linear,
        Exponential,
        EaseIn,
        EaseOut,
        Smoothstep,
        Smootherstep
    }

    public static IEnumerator MovePosition(Transform target, Vector3 position, float duration, Type type)
    {
        float timer = 0f;
        float t = 0f;
        Vector3 oldPos = target.position;
        Vector3 newPos;
        float closePoint = 0.001f;
        while (Mathf.Abs((target.position - position).sqrMagnitude) > closePoint)
        {
            timer += Time.deltaTime;
            t = Mathf.Clamp01(timer / duration);
            t = ModifyTransition(t, type);
            newPos = Vector3.Lerp(oldPos, position, t);
            target.position = newPos;
            yield return null;
        }
        target.position = position;
    }

    public static float Transition(float v0, float v1, ref float timer, float duration, Type type)
    {
        timer += Time.deltaTime;
        float t = Mathf.Clamp01(timer / duration);
        t = ModifyTransition(t, type);
        return Lerp(v0, v1, t);
    }

    public static float Lerp(float v0, float v1, float t)
    {
        return (1 - t) * v0 + t * v1;
    }

    public static float ModifyTransition(float t, Type type)
    {
        switch (type)
        {
            default: return t;
            case Type.Exponential: return t * t;
            case Type.EaseIn: return 1 - Mathf.Cos(t * Mathf.PI * 0.5f);
            case Type.EaseOut: return Mathf.Sin(t * Mathf.PI * 0.5f);
            case Type.Smoothstep: return t * t * (3f - 2f * t);
            case Type.Smootherstep: return t * t * t * (t * (6f * t - 15f) + 10f);
        }
    }

    public static IEnumerator WaitForAnimation(string name, Animator anim)
    {
        anim.Play(name);
        while (true)
        {
            var info = anim.GetCurrentAnimatorStateInfo(0);
            if (info.IsName(name) && (info.normalizedTime >= 1f || anim.IsInTransition(0)))
            {
                yield break;
            }
            yield return null;
        }
    }

    public static IEnumerator WaitForAnimation(string name, Animation anim)
    {
        anim.Play(name, PlayMode.StopAll);
        while (true)
        {
            if (anim[name].normalizedTime >= 1f) yield break;
            yield return null;
        }
    }

    public static IEnumerator WaitFor(float seconds)
    {
        float timer = seconds;
        while (timer > 0f)
        {
            timer -= Time.deltaTime;
            yield return null;
        }
    }

}
