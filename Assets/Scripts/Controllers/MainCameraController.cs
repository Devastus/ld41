﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCameraController : MonoBehaviour {

    public enum State
    {
        MainMenu,
        PlayerPlaceBall,
        PlayerAim,
        BallMoving,
        Wave
    }

    private const float MAX_WIGGLE = 1f;
    private const float MAX_FORCE_VALUE = 400f;

    public static System.Action<State> OnSetState;
    public static System.Action<Transform> OnSetTarget;
    public static System.Action<Vector3, float> OnWiggle;
    public static System.Action<Transform, Transform> OnCollision;

    public Transform target;
    public Transform mainMenuTarget;
    public Transform chargeMeshTransform;
    public float distance;
    public float ortographicSize;
    public Vector3 offset;
    public Vector3 rotation;
    public Vector2 yLock;
    public Vector2 distanceLock;
    public float moveSpeed = 1f;
    public float rotationSpeed = 1f;
    public float distanceSpeed = 1f;
    public float inputSpeed = 1f;

    [Header("Aiming Settings")]
    public LayerMask aimingMask;
    public float aimingDistance;
    public Vector2 aimOrtographicSize;

    [Header("Wiggle FX")]
    public float wiggleSpeed = 1f;
    public float wiggleIntensity = 1f;
    public float wiggleFalloffSpeed = 1f;

    private float _wiggleFactor = 0f;
    private float _wiggleTimer = 0f;

    private Vector3 _targetPosition;
    private Vector3 _rotationVector;
    private Vector3 _wiggleVector = Vector3.zero;
    private Vector3 _chargePos;
    private Quaternion _targetRotation;
    private Vector2 _input;
    private Vector3 _mouseWorldPosition;
    private float _yOffset;

    private State _state = State.MainMenu;
    private bool charging = false;
    private float charge = 0f;
    private float chargeMultiplier = 0.018f;
    RaycastHit hit;
    Camera cam;
    PlacerGizmo placerGizmo;
    MeshRenderer chargeMeshRenderer;
    Ball targetBall;

    private void OnEnable()
    {
        cam = GetComponent<Camera>();
        OnSetState = SetState;
        OnCollision = Collision;
        OnSetTarget = SetTarget;
        OnWiggle += ApplyWiggle;
        LevelManager.OnBallDeployed += DeployBall;
        chargeMeshRenderer = chargeMeshTransform.GetComponentInChildren<MeshRenderer>();
        _rotationVector = rotation;
    }

    private void OnDisable()
    {
        OnSetState = null;
        OnCollision = null;
        OnSetTarget = null;
        OnWiggle -= ApplyWiggle;
        LevelManager.OnBallDeployed -= DeployBall;
    }

    private void Update()
    {
        _input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        if (Input.GetMouseButton(1))
        {
            _input.x += Input.GetAxis("Mouse X");
            _input.y += Input.GetAxis("Mouse Y");
        }
        _mouseWorldPosition = GetPositionFromScreen();
        _rotationVector = new Vector3(Mathf.Clamp(_rotationVector.x - _input.y * inputSpeed, yLock.x, yLock.y), _rotationVector.y + _input.x * inputSpeed, rotation.z);
        _yOffset = Mathf.InverseLerp(yLock.y, yLock.x, _rotationVector.y) * offset.y;
        distance = Mathf.Clamp(distance + Input.GetAxis("Mouse ScrollWheel") * 100f, distanceLock.x, distanceLock.y);

        //if (Input.GetButtonDown("Menu")) GameManager.UIManager.gameWindow.TogglePause();

        if (_state == State.PlayerAim)
        {
            //Enter charge mode
            if (Input.GetMouseButtonDown(0))
            {
                _chargePos = _mouseWorldPosition;
                charging = true;
                GameManager.LevelManager.aimAssist.SetLine(target, _chargePos, targetBall.Collider.center.y, targetBall.Collider.radius);
                GameManager.LevelManager.aimAssist.SetPosition(target, _chargePos, 0.5f);
                GameManager.LevelManager.aimAssist.SetChargeVisible(true);
            }
            if (charging)
            {
                // GameManager.LevelManager.aimAssist.SetLine(target, _chargePos, targetBall.Collider.radius);
                
                Vector3 launchDirection = (_chargePos - target.position).normalized;
                Vector3 mouseDir = (_chargePos - _mouseWorldPosition).normalized;
                float dot = Mathf.Clamp(Vector3.Dot(launchDirection, mouseDir), 0, 1);
                float chargeDistance = (_chargePos - _mouseWorldPosition).magnitude * dot;
                charge = Mathf.Clamp(chargeDistance * chargeMultiplier, 0f, 1f);
                GameManager.LevelManager.aimAssist.SetCharge(charge);

                //SetChargePower(charge);
                //SetChargePosition(target.transform.position);
                if (Input.GetMouseButtonUp(0))
                {
                    GameManager.LevelManager.aimAssist.SetChargeVisible(false);
                    charging = false;
                    float force = MAX_FORCE_VALUE * charge;
                    //Vector3 dir = Vector3.ProjectOnPlane(direction.normalized, Vector3.up).normalized;
                    PlayerBall pb = target.GetComponent<PlayerBall>();
                    pb.Launch(launchDirection, force);
                    GameManager.SoundManager.PlaySFX(pb.launchBallSound, true, null, 0.6f, 1f, 0.2f);
                    OnSetState(State.BallMoving);
                    
                }
            } else
            {
                GameManager.LevelManager.aimAssist.SetLine(target, _mouseWorldPosition, targetBall.Collider.center.y, targetBall.Collider.radius);
            }

        }
        else if (_state == State.BallMoving)
        {
            bool moving = GameManager.LevelManager.IsAnyBallMoving();
            if (!moving)
            {
                //Check if we have more tries, otherwise change state
                if (!GameManager.LevelManager.UseTry()) OnSetState(State.Wave);
                else OnSetState(State.PlayerAim);
                //else if (GameManager.LevelManager.deployables.Count > 0)
                //{
                //    OnSetState(State.PlayerPlaceBall);
                //}
                //else
                //{
                //    OnSetState(State.PlayerAim);
                //}
            }
        }
    }

    // Update is called once per frame
    void LateUpdate () {
        UpdateWiggle();
        StateLogic();
        transform.rotation = _targetRotation;
        transform.position = _targetPosition;
    }

    private void StateLogic()
    {
        _targetRotation = Quaternion.Slerp(_targetRotation, Quaternion.Euler(_rotationVector), Time.deltaTime * rotationSpeed);
        switch (_state)
        {
            default:
                //_targetRotation = Quaternion.Slerp(_targetRotation, Quaternion.Euler(_rotationVector), Time.deltaTime * rotationSpeed);

                //_targetRotation = Quaternion.Slerp(_targetRotation, Quaternion.LookRotation(-rotation.normalized, Vector3.up), Time.deltaTime * rotationSpeed);
                cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, ortographicSize, Time.deltaTime * distanceSpeed);
                if (target != null)
                {
                    _targetPosition = Vector3.Lerp(_targetPosition, target.position + offset + (_targetRotation * Vector3.forward) * distance, Time.deltaTime * moveSpeed);
                } else
                {
                    _targetPosition = Vector3.Lerp(_targetPosition, offset + (_targetRotation * Vector3.forward) * distance, Time.deltaTime * moveSpeed);
                }
                break;
            //case State.MainMenu:
            //    //_targetPosition = Vector3.Lerp(_targetPosition, mainMenuTarget.position, Time.deltaTime * speed);
            //    break;
            //case State.PlayerPlaceBall:
            //    {
            //        Vector3 mouseDir = (mouseWorldPosition - Vector3.zero);
            //        float dist = mouseDir.magnitude;
            //        Vector3 modPos = mouseDir.normalized * Mathf.Clamp(dist * 0.1f, 0f, 50f);
            //        _targetPosition = Vector3.Lerp(_targetPosition, modPos + (_targetRotation * Vector3.forward) * distance, Time.deltaTime * speed);
            //    }
            //    break;
            case State.PlayerAim:
                if (target != null)
                {
                    //_targetRotation = Quaternion.Slerp(_targetRotation, Quaternion.Euler(_rotationVector), Time.deltaTime * rotationSpeed);
                    Vector3 mouseDir = (_mouseWorldPosition - target.position);
                    float dist = mouseDir.magnitude;
                    float mod = Mathf.Clamp(dist * 0.006f, 0f, 1f);
                    Vector3 modPos = target.position + mouseDir.normalized * mod;
                    //float size = Mathf.Lerp(aimOrtographicSize.x, aimOrtographicSize.y, mod);
                    //cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, size, Time.deltaTime * distanceSpeed);
                    _targetPosition = Vector3.Lerp(_targetPosition, modPos + offset + (_targetRotation * Vector3.forward) * distance, Time.deltaTime * moveSpeed);
                }
                break;
            case State.BallMoving:
                //cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, ortographicSize, Time.deltaTime * distanceSpeed);
                //Vector3 turrets = GameManager.LevelManager.GetAverageTurretPos();
                //Vector3 average = (turrets + target.position) * 0.5f;
                _targetPosition = Vector3.Lerp(_targetPosition, target.position + offset +(_targetRotation * Vector3.forward) * distance, Time.deltaTime * moveSpeed);
                //if(target != null)
                //{
                //    _targetPosition = Vector3.Lerp(_targetPosition, target.transform.position + direction.normalized * distance, Time.deltaTime * speed);
                //}
                break;
            case State.Wave:
                //cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, ortographicSize, Time.deltaTime * distanceSpeed);
                
                
                Vector3 enemies = GameManager.LevelManager.GetAverageEnemyPos();
                //Vector3 diff = enemies - target.position;
                //float averageDistance = Mathf.Min(diff.sqrMagnitude, distance);
                Vector3 midPoint = (enemies + target.position) * 0.5f;
                _targetPosition = Vector3.Lerp(_targetPosition, midPoint + offset + (_targetRotation * Vector3.forward) * distance, Time.deltaTime * moveSpeed);
                break;
        }
        _targetPosition += _wiggleVector;
    }

    void SetTarget(Transform t)
    {
        //Debug.Log("MainCameraController.SetTarget(): " + t.name);
        target = t;
        targetBall = target.GetComponent<Ball>();
    }

    void SetState(State state)
    {
        this._state = state;
        switch (_state)
        {
            case State.MainMenu:
                target = null;
                targetBall = null;
                break;
            case State.PlayerPlaceBall:
                //GetGizmo();
                target = null;
                targetBall = null;
                break;
            case State.PlayerAim:
                target = GameManager.LevelManager.playerBall.transform;
                targetBall = target.GetComponent<Ball>();
                GameManager.LevelManager.aimAssist.SetActive(true);
                break;
            case State.BallMoving:
                GameManager.LevelManager.aimAssist.SetActive(false);
                targetBall = target.GetComponent<Ball>();
                break;
            case State.Wave:
                target = GameManager.LevelManager.playerBall.transform;
                targetBall = target.GetComponent<Ball>();
                break;
        }
    }

    void ApplyWiggle(Vector3 position, float force)
    {
        _wiggleFactor = Mathf.Clamp(_wiggleFactor + force, 0, MAX_WIGGLE);
    }

    void UpdateWiggle()
    {
        _wiggleTimer = (_wiggleTimer + (Time.deltaTime * wiggleSpeed)) % (Mathf.PI * 2 * wiggleSpeed);
        _wiggleFactor = Mathf.Clamp(_wiggleFactor - (Time.deltaTime * wiggleFalloffSpeed), 0, MAX_WIGGLE);
        _wiggleVector = new Vector3(Mathf.Sin(_wiggleTimer * 0.81f),
                                   Mathf.Sin((_wiggleTimer + 0.04f) * 0.78f),
                                   Mathf.Sin((_wiggleTimer + 0.07f) * 0.62f)) * _wiggleFactor * wiggleIntensity;
    }

    private void Collision(Transform target1, Transform target2)
    {
        target = null;
    }

    private void DeployBall(Ball ball)
    {
        System.Type type = ball.GetType();
        if (type == typeof(PlayerBall))
        {
            //FUCK IT
        } else
        {
            OnSetState(State.PlayerAim);
        }
    }

    private void GetAimAssist()
    {
        GameManager.LevelManager.aimAssist.SetActive(true);
    }

    private void SetChargeVisible(bool visible)
    {
        chargeMeshRenderer.enabled = visible;
    }

    private void SetChargePower(float value)
    {
        chargeMeshRenderer.sharedMaterial.SetFloat("_Alpha", value);
    }

    private void SetChargePosition(Vector3 position)
    {
        chargeMeshRenderer.transform.rotation = transform.rotation;
        chargeMeshRenderer.transform.position = position - transform.forward * 1f;
    }

    private Vector3 GetPositionFromScreen()
    {
        //Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        //if (Physics.Raycast(ray, out hit, 200f, aimingMask))
        //{
        //    return hit.point;
        //}
        //return Vector3.zero;
        Plane plane = new Plane(Vector3.up, 0f);
        float intersectDistance;
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);

        if (plane.Raycast(ray, out intersectDistance))
        {
            return ray.origin + (ray.direction * intersectDistance);
        }
        return Vector3.zero;
    }
}
