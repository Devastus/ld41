﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SimpleFSM
{
    [System.Serializable, CreateAssetMenu(menuName = "Simple State Machine")]
    public class SimpleStateMachine : ScriptableObject
    {
        /* NOTE:    This is a simple State Machine for sequencing things like gameplay states and such.
         *          States are custom-written ScriptableObjects that have Start, Run and End processes.
         *          Transitions between States are specified in the editor, and automatically run after End Cycles unless custom transitions are specified in code.
         *          
         */

        [SerializeField, Tooltip("States are run in order starting from 0")]
        public List<State> states = new List<State>();

        MonoBehaviour _parent;                          //Parent MonoBehaviour component
        Transition _transition;                         //Current transition (if any)
        IEnumerator _currentCycle;                      //Current Cycle(Routine) of the SequenceState
        int _currentStateIndex;

        private State _currentState;                    //Current SequenceState to run
        public State CurrentState { get { return _currentState; } }
        private bool _running = false;                  //Is the state machine running
        public bool Running { get { return _running; } }
        private bool _inTransition = false;             //Is the state machine currently in transition
        public bool InTransition { get { return _inTransition; } }

        public IEnumerator Run(MonoBehaviour parent)
        {
            if(states.Count <= 0)
            {
                Debug.LogError(name+".Run(): No states found! State Machine must have at least one state to run.");
                yield break;
            }
            Initialize(parent);
            _running = true;
            while (_running)
            {
                if (!_inTransition)
                {
                    //Run our current State Cycle
                    if (_currentCycle.MoveNext())
                    {
                        yield return _currentCycle.Current;
                    }
                    //Switch to either next Cycle, or State
                    else
                    {
                        if (_currentState.CurrentCycle != State.Cycle.End)
                        {
                            _currentState.NextCycle();
                            _currentCycle = _currentState.GetCycleRoutine(this);
                        }
                        else
                        {
                            if (states[_currentStateIndex].nextStateIndices.Length > 0 && states[_currentStateIndex].NextStateIndex > State.EMPTY_TRANSITION)
                            {
                                NextState();
                            }
                            else
                            {
                                _running = false;
                                break;
                            }
                        }
                    }
                }
                //We have a transition activated
                else
                {
                    //If current Cycle is not finished, keep running the Cycle
                    if (_currentCycle.MoveNext())
                    {
                        yield return _currentCycle.Current;
                    } 
					//If we're not at the end and the Transition Type is Safe, continue to the next Cycle
					else if(_transition.type == SimpleFSM.Transition.Type.Safe && _currentState.CurrentCycle != State.Cycle.End)
					{
						_currentState.NextCycle();
                        _currentCycle = _currentState.GetCycleRoutine(this);
					}
                    //Otherwise switch to a new State
                    else
                    {
                        _currentState = _transition.to;
                        _inTransition = false;
                        if (_currentState != null)
                        {
                            _currentStateIndex = _transition.stateIndex;
                            _currentState.ResetCycle();
                            _currentCycle = _currentState.GetCycleRoutine(this);
                        }
                    }
                }
            }
        }

        private void Initialize(MonoBehaviour parent)
        {
            this._parent = parent;
            _currentStateIndex = 0;
            _currentState = GetState();
            _currentState.ResetCycle();
            _currentCycle = _currentState.GetCycleRoutine(this);
        }

        public void Stop()
        {
            _running = false;
        }

        public void Transition(int index, SimpleFSM.Transition.Type type = SimpleFSM.Transition.Type.Safe)
        {
            if (index >= 0 && index < states.Count)
            {
                _transition = new SimpleFSM.Transition(_currentState, states[index], index, type);
                _inTransition = true;
            }
        }

        private void NextState()
        {

            _currentStateIndex = states[_currentStateIndex].NextStateIndex;
            _currentState = GetState();
            _currentState.ResetCycle();
            _currentCycle = _currentState.GetCycleRoutine(this);
        }
		
		public State GetState(int index)
		{
			if (index >= 0 && index < states.Count)
			{
				return states[index];
			}
			return null;
		}

        public string[] GetStateNames()
        {
            int length = states.Count + 1;
            string[] result = new string[length];
            result[0] = "None";
            for (int i = 1; i < length; i++)
            {
                result[i] = states[i-1].name;
            }
            return result;
        }

        private State GetState()
        {
            return states[_currentStateIndex];
        }

    }
}

